package com.example.studies_help.ui.learningPlanner;



import com.example.myapplication.ui.learningPlanner.Calendar.Day;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class DayTest {

    private Day day;

    @Before
    public void initDay() {
      day = new Day("31.01.2021");
    }


    @Test
    public void testSetAnOtherDate() {
        day.setAnOtherDate("09.02.2021");
        assertEquals("09.02.2021", day.getCompleteDate());
        day.setAnOtherDate("20.01.2021");
        assertEquals("20.01.2021", day.getCompleteDate());
    }
    @Test
    public void testGetDateDifference() throws ParseException {
        assertEquals(3, day.getDayDifference("23.02.2021", "26.02.2021"));
        assertEquals(3, day.getDayDifference("28.02.2021", "03.03.2021"));

    }

    @Test
    public void testGetFirstDayOfTheWeek() throws ParseException {
        assertEquals("07.02.2021", day.getFirstDayOfTheWeek("09.02.2021"));
        assertEquals("17.01.2021", day.getFirstDayOfTheWeek("20.01.2021"));
    }
}
