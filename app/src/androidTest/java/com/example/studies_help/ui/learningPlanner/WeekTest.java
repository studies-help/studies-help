package com.example.studies_help.ui.learningPlanner;

import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.Calendar.Week;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class WeekTest {
    Week week;
    @Before
    public void initWeek(){
        week= new Week("01.02.2021");
    }

    @Test
    public void generateWeekTest(){
        List<Day> days = new ArrayList<>();
        days.add(new Day("01.02.2021"));
        days.add(new Day("02.02.2021"));
        days.add(new Day("03.02.2021"));
        days.add(new Day("04.02.2021"));
        days.add(new Day("05.02.2021"));
        days.add(new Day("06.02.2021"));
        days.add(new Day("07.02.2021"));
        for(int i= 0; i<7 ; i++)
        assertEquals(days.get(i).getCompleteDate(), week.getDaysOfTheWeek().get(i).getCompleteDate());
    }

    @Test
    public void generateNextWeekTest(){
        List<Day> days = new ArrayList<>();
        days.add(new Day("08.02.2021"));
        days.add(new Day("09.02.2021"));
        days.add(new Day("10.02.2021"));
        days.add(new Day("11.02.2021"));
        days.add(new Day("12.02.2021"));
        days.add(new Day("13.02.2021"));
        days.add(new Day("14.02.2021"));

        List<Day> generatedDays =  week.getDaysOfTheNextWeek();
        for(int i= 0; i<7 ; i++)
            assertEquals(days.get(i).getCompleteDate(), generatedDays.get(i).getCompleteDate());
    }

    @Test
    public void generateLastWeekTest(){
        List<Day> days = new ArrayList<>();

        days.add(new Day("25.01.2021"));
        days.add(new Day("26.01.2021"));
        days.add(new Day("27.01.2021"));
        days.add(new Day("28.01.2021"));
        days.add(new Day("29.01.2021"));
        days.add(new Day("30.01.2021"));
        days.add(new Day("31.01.2021"));
        List<Day> generatedDays =  week.getDaysOfTheLastWeek();
        for(int i= 0; i<7 ; i++)
            assertEquals(days.get(i).getCompleteDate(), generatedDays.get(i).getCompleteDate());
    }

}

