package com.example.myapplication.ui.learningPlanner;

import com.example.myapplication.data.Course;
import com.example.myapplication.data.EventLearningPlanner;

import java.util.List;

public interface EventLearningPlannerDataSourceInterface {
    void updateData(EventLearningPlanner event);

    void addData(EventLearningPlanner event);

    List<EventLearningPlanner> getEventDuringHour(String date, String time);

    List<EventLearningPlanner> getEventStartAtHour(String date, String time);

    List<EventLearningPlanner> getEventAtDate(String date);

    List<EventLearningPlanner> getEventDuringTimePeriod(String date, String startTime, String endTime);

    void deleteEvent(EventLearningPlanner event);

    List<Course> getCourse(Integer id);

    List<Course> getCoursesForCourseChoice();
}
