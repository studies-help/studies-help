package com.example.myapplication.ui.learningPlanner.fragments.DayChoose;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.Calendar.Week;
import com.example.myapplication.ui.learningPlanner.adapter.DayChooseGridAdapter;
import com.example.myapplication.ui.learningPlanner.fragments.DayOverview.DayOverviewFragment;
import com.example.myapplication.ui.learningPlanner.fragments.Weekoverview.WeekOverviewFragment;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to load Overview of All Days of on Week and set DayOverview or WeekOverview
 */
public class DayChooseOverviewFragment extends Fragment {
    private List<Day> daysOfTheWeek = new ArrayList<>();
    private int lastPositionOfChosenDay;
    private String date;
    private String chosenDate;
    private FragmentManager fm;
    private Week thisWeek;


    public DayChooseOverviewFragment() {
    }


    public DayChooseOverviewFragment(String date, String chosenDay) {
        this.date = date;
        this.chosenDate = chosenDay;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fm = getParentFragmentManager();

        Log.d("DayChooseOverviewFragment", "Load new Week");
        // Inflate the layout for this fragment
        if (date == null)
            thisWeek = new Week();
        else
            thisWeek = new Week(date);
        this.daysOfTheWeek = thisWeek.getDaysOfTheWeek();
        String dateOfTheFirstDayOfTheWeek = this.daysOfTheWeek.get(0).getCompleteDate();

        View root = inflater.inflate(R.layout.fragment_learning_planner_day_choose_overview, container, false);

        if (chosenDate == null) {
            switchFragment(new DayOverviewFragment(daysOfTheWeek.get(0).getCompleteDate()));
            lastPositionOfChosenDay = 0;
        } else {
            try {
                Long theLong = new Day().getDayDifference(dateOfTheFirstDayOfTheWeek, chosenDate);
                lastPositionOfChosenDay = theLong != null ? theLong.intValue() : null;
            } catch (ParseException e) {
                lastPositionOfChosenDay = 0;
                Log.e("DayChoseOverViewFragment", "Unable to get distance between Dates. Set Default Value Exception:" + e);
            }
            switchFragment(new DayOverviewFragment(chosenDate));
            chosenDate = null;
        }
        final GridView gridView = root.findViewById(R.id.learningPlannerDay_Grid);
        setGridView(this.daysOfTheWeek, gridView);

        final ImageButton nextWeekButton = root.findViewById(R.id.learningPlannerNextWeekButton);
        nextWeekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Day> days;
                days = thisWeek.getDaysOfTheNextWeek();

                setGridView(days, gridView);
                loadFragment(days);
            }
        });


        final ImageButton lastWeekButton = root.findViewById(R.id.learningPlannerLastWeekButton);
        lastWeekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Day> days;
                days = thisWeek.getDaysOfTheLastWeek();
                setGridView(days, gridView);
                loadFragment(days);
            }
        });

        final Button setToday = getActivity().findViewById(R.id.learningPlannerSetTodayButton);
        setToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Day> days;
                thisWeek.setThisWeek();
                days = thisWeek.getDaysOfTheWeek();
                lastPositionOfChosenDay= 0;
                setGridView(days, gridView);
                loadFragment(days);
            }
        });

        Switch sw = root.findViewById(R.id.learningPlannerSwitchTimePeriodSwitch);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                List<Fragment> fragments = getParentFragmentManager().getFragments();

                if (isChecked)
                    switchFragment(new DayOverviewFragment(daysOfTheWeek.get(lastPositionOfChosenDay).getCompleteDate()));
                else
                    switchFragment(new WeekOverviewFragment(daysOfTheWeek));
            }
        });
        return root;
    }


    private void loadFragment(List<Day> days) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        List<Fragment> fragments = getParentFragmentManager().getFragments();
        for (Fragment e : fragments) {
            if (e != null) {
                if (e instanceof DayOverviewFragment && days.size() > 0)
                    fragmentTransaction.replace(R.id.learningPlannerDayChooseFragmentPeriodSwitch, new DayOverviewFragment(days.get(lastPositionOfChosenDay).getCompleteDate()));
                else if (e instanceof WeekOverviewFragment && days.size() > 6)
                    fragmentTransaction.replace(R.id.learningPlannerDayChooseFragmentPeriodSwitch, new WeekOverviewFragment(days));
            }
        }
        fragmentTransaction.commit(); // save the changes
    }

    private void setGridView(List<Day> week, GridView gridView) {
        gridView.setAdapter(new DayChooseGridAdapter(getActivity(), week, lastPositionOfChosenDay));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView day = view.findViewById(R.id.learningPlannerDayChooseDateTextView);
                day.setBackgroundColor(Color.parseColor("#00343434"));
                if (lastPositionOfChosenDay != position)
                    gridView.getChildAt(lastPositionOfChosenDay).findViewById(R.id.learningPlannerDayChooseDateTextView).setBackgroundColor(Color.parseColor("#10C1ED"));
                lastPositionOfChosenDay = position;
                loadFragment(week);
            }
        });
    }

    private void switchFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.learningPlannerDayChooseFragmentPeriodSwitch, fragment);
        fragmentTransaction.commit();
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        daysOfTheWeek = new Week(date).getDaysOfTheWeek();
    }

    public List<Day> getDaysOfTheWeek() {
        return daysOfTheWeek;
    }

    public void setDaysOfTheWeek(List<Day> daysOfTheWeek) {
        this.daysOfTheWeek = daysOfTheWeek;
    }
}