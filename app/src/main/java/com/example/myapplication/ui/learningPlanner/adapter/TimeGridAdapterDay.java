package com.example.myapplication.ui.learningPlanner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.DayOverview;
import com.example.myapplication.ui.learningPlanner.TimeStamp;
import com.example.myapplication.ui.learningPlanner.fragments.LearningPlannerEditFragment;
import com.example.myapplication.ui.learningPlanner.modifiedGridView.MyGridView;

/**
 * adapter to fill Grid of DayOverViewFragment with Timestamp-Elements
 */

public class TimeGridAdapterDay extends BaseAdapter {
    private final DayOverview dayoverview;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private final Day today = new Day();
    private Boolean isToday;
    private final FragmentManager fragmentManager;


    public TimeGridAdapterDay(Context aContext, DayOverview dayoverview, FragmentManager fragmentManager) {
        this.context = aContext;
        this.dayoverview = dayoverview;

        layoutInflater = LayoutInflater.from(aContext);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return dayoverview.getTimestamps().size();
    }

    @Override
    public Object getItem(int position) {
        return dayoverview.getTimestamps().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View week_listview, ViewGroup parent) {
        ViewHolderTime holderTime;

        TimeStamp timeStamp = dayoverview.getTimestamps().get(position);
        if (week_listview == null) {
            week_listview = layoutInflater.inflate(R.layout.fragment_learning_planner_event_day_overview_time_stamp_griedview, null);
            holderTime = new ViewHolderTime();

            holderTime.time = week_listview.findViewById(R.id.learningPlannerDayTimeTextView);
            week_listview.setTag(holderTime);

        } else
            holderTime = (ViewHolderTime) week_listview.getTag();
        holderTime.time.setText(timeStamp.getTimeStamp());

        MyGridView myGridView = week_listview.findViewById(R.id.learningPlannerDayEventGrid);

        myGridView.setAdapter(new EventDayGridAdapter(context, timeStamp.getEvents()));
        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TimeGridAdapter", "Choose event");
                LearningPlannerEditFragment learningPlannerEditFragment = LearningPlannerEditFragment.newInstance(timeStamp.getEvents().get(position), fragmentManager);
                learningPlannerEditFragment.show(fragmentManager, "learningPlannerEditFragment");
            }
        });

        if (isNow(timeStamp.getTimeStamp(), dayoverview.getDate()))
            week_listview.findViewById(R.id.learningPlannerTimeStampStringTextView).setBackgroundColor(Color.parseColor("#FF9800"));
        else
            week_listview.findViewById(R.id.learningPlannerTimeStampStringTextView).setBackgroundColor(Color.parseColor("#ACACAC"));
        return week_listview;
    }

    static class ViewHolderTime {
        TextView time;
    }

    public Boolean getIsToday() {
        return isToday;
    }

    public void setIsToday(Boolean today) {
        this.isToday = today;
    }

    public Boolean isNow(String timeOfEvent, String date) {
        String timeNow = today.getTime();
        String dateNow = today.getCompleteDate();
        return timeOfEvent.equals(timeNow) && dateNow.equals(date);
    }
}

