package com.example.myapplication.ui.learningPlanner.fragments.Weekoverview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.WeekOverview;
import com.example.myapplication.ui.learningPlanner.adapter.TimeGridAdapterWeek;

import java.util.List;


public class WeekOverviewFragment extends Fragment {
    private final List<Day> daysOfTheWeek;
    private WeekOverview weekOverview;

    public WeekOverviewFragment(List<Day> week) {
        this.daysOfTheWeek = week;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weekOverview = new WeekOverview(getActivity(), daysOfTheWeek);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_learning_planner_week_overview, container, false);
        final GridView gridView = (GridView) root.findViewById(R.id.LearningPlannerTimeStampsGrid);
        gridView.setAdapter(new TimeGridAdapterWeek(getActivity(), weekOverview, getParentFragmentManager()));
        return root;
    }
}