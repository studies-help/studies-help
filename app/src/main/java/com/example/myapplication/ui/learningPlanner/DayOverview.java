package com.example.myapplication.ui.learningPlanner;


import android.content.Context;
import android.util.Log;

import com.example.myapplication.data.EventLearningPlanner;
import com.example.myapplication.ui.learningPlanner.Calendar.Hour;

import java.util.ArrayList;
import java.util.List;

/**
 * Class with all Timestamps with events for one day
 */

public class DayOverview {
    private final String date;
    private List<TimeStamp> timestamps = new ArrayList<>();
    List<EventLearningPlanner> events = new ArrayList<>();

    public DayOverview(Context context, String date) {
        EventLearningPlannerDataSource dataSource = new EventLearningPlannerDataSource(context);
        this.date = date;
        findEventsPerHoursOfTheDay(date, dataSource);
    }

    private void findEventsPerHoursOfTheDay(String date, EventLearningPlannerDataSource dataSource) {
        for (int counter = 1; counter < 25; counter++) {
            String time = new Hour(counter).getFullHour();
            List<EventLearningPlanner> current = dataSource.getEventStartAtHour(date, new Hour(counter).getOnlyHour());
            Log.d("DayOverView", "find " + current.size() + "events at" + counter + ":00");
            timestamps.add(new TimeStamp(time, current));
            if (current.size() != 0) {
                counter = getLatestTime(current);
            }
        }
    }

    public Integer getLatestTime(List<EventLearningPlanner> current) {
        Integer latestTime = 0;
        Integer currentTimeHours;
        Boolean isCompleteHour = false;
        for (EventLearningPlanner event : current) {
            currentTimeHours = Integer.parseInt(event.getEventEndTime().substring(0, 2));
            isCompleteHour = event.getEventEndTime().startsWith("00", 3);
            if (latestTime < currentTimeHours) {
                latestTime = currentTimeHours;
            }
        }

        if (isCompleteHour)
            return latestTime;
        else
            return latestTime + 1;
    }

    public List<TimeStamp> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(List<TimeStamp> timestamps) {
        this.timestamps = timestamps;
    }

    public List<EventLearningPlanner> getEvents() {
        return events;
    }

    public void setEvents(List<EventLearningPlanner> events) {
        this.events = events;
    }

    public String getDate() {
        return date;
    }
}
