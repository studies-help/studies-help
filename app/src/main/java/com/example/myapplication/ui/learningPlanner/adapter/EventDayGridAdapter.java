package com.example.myapplication.ui.learningPlanner.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.data.EventLearningPlanner;
import com.example.myapplication.ui.learningPlanner.EventLearningPlannerDataSource;

import java.util.List;

/**
 * adapter to fill GridView in GridElement  of TimeGridDay with EventInformation
 */

public class EventDayGridAdapter extends BaseAdapter {
    private final LayoutInflater layoutInflater;
    private final List<EventLearningPlanner> eventsAfterFullHour;
    private final Context context;

    public EventDayGridAdapter(Context aContext, List<EventLearningPlanner> eventsAfterFullHour) {
        Log.d("EventGridAdapter", "adapter was called Listengröße:" + eventsAfterFullHour.size());
        layoutInflater = LayoutInflater.from(aContext);
        this.eventsAfterFullHour = eventsAfterFullHour;
        this.context = aContext;
    }

    @Override
    public int getCount() {
        return eventsAfterFullHour.size();
    }

    @Override
    public Object getItem(int position) {
        return eventsAfterFullHour.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View gridViewEventExist, ViewGroup parent) {

        ViewHolderEvent holderEvent;
        EventLearningPlanner event = eventsAfterFullHour.get(position);

        if (gridViewEventExist == null) {
            gridViewEventExist = layoutInflater.inflate(R.layout.fragment_learning_planner_day_overview_event_gridview, null);

            holderEvent = new ViewHolderEvent();

            holderEvent.startTimeOfEvent = gridViewEventExist.findViewById(R.id.learningPlannerWeekStartTimeOfEventTextView);
            holderEvent.endTimeOfEvent = gridViewEventExist.findViewById(R.id.learningPlannerWeekendTimeOfEventTextView);
            holderEvent.eventName = gridViewEventExist.findViewById(R.id.learningPlannerWeekEventNameTextView);
            holderEvent.involved = gridViewEventExist.findViewById(R.id.learningPlannerDayInvolvedTextView);
            holderEvent.courseName = gridViewEventExist.findViewById(R.id.learningPlannerDayCourseNameTextView);
            gridViewEventExist.setTag(holderEvent);
        } else
            holderEvent = (ViewHolderEvent) gridViewEventExist.getTag();
        
        Log.d("Event", "matching event found");
        holderEvent.eventName.setText(event.getEventName());
        holderEvent.startTimeOfEvent.setText(event.getEventStartTime());
        holderEvent.endTimeOfEvent.setText(event.getEventEndTime());
        holderEvent.courseName.setText(event.getCourseID() == null ? "" : new EventLearningPlannerDataSource(context).getCourse(event.getCourseID()).get(0).getCourseName());
        holderEvent.involved.setText(event.getEventInvolved());

        return gridViewEventExist;
    }


    static class ViewHolderEvent {

        TextView startTimeOfEvent;
        TextView endTimeOfEvent;
        TextView eventName;
        TextView involved;
        TextView courseName;
    }
}
