package com.example.myapplication.ui.learningCards;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.data.AppDatabase;
import com.example.myapplication.data.DbSingleton;
import com.example.myapplication.data.LearningCard;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


/**
 * Fragment where the titles of learning cards are displayed
 * by clicking on + button a learning card can be created
 * by clicking on a learning card this card can be edited
 */

public class LearningCardsFragment extends Fragment {
    private AppDatabase appDatabase;
    private List<LearningCard> learningCardList;
    private ArrayAdapter<LearningCard> arrayAdapter;
    private ImageButton searchButton;
    private EditText searchEditText;
    private View root;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() == null)
            return root;
        root = inflater.inflate(R.layout.fragment_learning_cards_main, container, false);
        FloatingActionButton addButton = getActivity().findViewById(R.id.fab);
        addButton.setImageResource(R.drawable.ic_learning_cards_add);
        addButton.setVisibility(View.VISIBLE);
        addButton.setOnClickListener(this::onClickAddButton);
        root = inflater.inflate(R.layout.fragment_learning_cards_main, container, false);
        searchEditText = root.findViewById(R.id.searchEditText);
        searchEditText.setVisibility(View.GONE);
        ((ListView) root.findViewById(R.id.displayLearningCardsListView)).setOnItemClickListener(this::onItemClickListView);
        ((ListView) root.findViewById(R.id.displayLearningCardsListView)).setOnItemLongClickListener(this::onItemLongClickListView);
        searchButton = root.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this::onClickSearchButton);
        searchButton.setTag(false);
        displayList();

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                arrayAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        return root;
    }


    private void onClickAddButton(View view) {
        Intent intent = new Intent(getActivity(), LearningCardsEditorActivity.class)
                .putExtra("learningCardID", Long.MIN_VALUE)
                .putExtra("learningCardTitle", "Titel")
                .putExtra("learningCardContent", "")
                .putExtra("learningCardContentAlignment", Gravity.START);
        startActivity(intent);
        Log.d("LearningCardsFragment", "open editor activity");
    }


    private void onClickSearchButton(View view) {
        boolean tag = (boolean) searchButton.getTag();
        if (tag) {
            searchEditText.setVisibility(View.GONE);
            searchButton.setTag(false);
        } else {
            searchEditText.setVisibility(View.VISIBLE);
            searchButton.setTag(true);
        }
    }


    /**
     * sets learning cards from database to ListView
     */

    @Override
    public void onResume() {
        super.onResume();
        displayList();
    }


    /**
     * displays all learning cards from database in the ListView
     */

    private void displayList() {
        this.appDatabase = DbSingleton.instance(getContext()).getAppDatabase();
        learningCardList = appDatabase.learningCardDao().getLearningCards();
        int learningCardListLength = learningCardList.size();
        LearningCard[] learningCards = new LearningCard[learningCardListLength];
        for (int i = 0; i < learningCardListLength; i++)
            learningCards[i] = learningCardList.get(i);
        arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, learningCards);
        ListView learningCardListView = root.findViewById(R.id.displayLearningCardsListView);
        learningCardListView.setAdapter(arrayAdapter);
    }


    /**
     * edits selected learning card
     *
     * @param parent   parent view
     * @param view     current view
     * @param position position of selected learning card
     * @param id       id of selected learning card
     */

    private void onItemClickListView(AdapterView<?> parent, View view, int position, long id) {
        LearningCard learningCard = (LearningCard) parent.getItemAtPosition(position);
        Intent intent = new Intent(getActivity(), LearningCardsEditorActivity.class)
                .putExtra("learningCardTitle", learningCard.getLearningCardTitle())
                .putExtra("learningCardContent", learningCard.getLearningCardContent())
                .putExtra("learningCardID", learningCard.getLearningCardID())
                .putExtra("learningCardContentAlignment", learningCard.getLearningCardContentAlignment());
        appDatabase.learningCardDao().delete(learningCard);
        arrayAdapter.notifyDataSetChanged();
        startActivity(intent);
        Log.d("LearningCardsFragment", "open editor with existing learn card");
    }


    /**
     * deletes selected item from listView and related learning card from database
     *
     * @param parent   parent view
     * @param view     current view
     * @param position position of selected item
     * @param id       id of selected item
     * @return true
     */

    private boolean onItemLongClickListView(AdapterView<?> parent, View view, int position, long id) {
        this.appDatabase = DbSingleton.instance(getContext()).getAppDatabase();
        new AlertDialog.Builder(requireActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Löschen?")
                .setMessage("Sind Sie sicher, dass die Notitz gelöscht werden soll?\n" +
                        "Dieser Vorgang kann nicht rückgängig gemacht werden!")
                .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        appDatabase.learningCardDao().delete(learningCardList.get(position));
                        learningCardList.remove(position);
                        arrayAdapter.notifyDataSetChanged();
                        displayList();
                    }
                }).setNegativeButton("Nicht löschen", null)
                .show();
        return true;
    }
}