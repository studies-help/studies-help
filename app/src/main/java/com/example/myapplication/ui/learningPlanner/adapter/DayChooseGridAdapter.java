package com.example.myapplication.ui.learningPlanner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;

import java.util.List;

/**
 * Class to fill up DayChooseGrid with Date from a whole Week
 */

public class DayChooseGridAdapter extends BaseAdapter {
    private final List<Day> daysOfOneWeek;
    private final LayoutInflater layoutInflater;
    private final Day today = new Day();
    private Day chosenDay;
    private final Integer chosenDayIndex;

    public DayChooseGridAdapter(Context aContext, List<Day> daysOfOneWeek, Integer firstDay) {
        this.daysOfOneWeek = daysOfOneWeek;
        layoutInflater = LayoutInflater.from(aContext);
        this.chosenDayIndex = firstDay;
    }


    @Override
    public int getCount() {
        return daysOfOneWeek.size();
    }

    @Override
    public Object getItem(int position) {
        return daysOfOneWeek.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View weekListView, ViewGroup parent) {
        ViewHolder holder;
        if (weekListView == null) {
            weekListView = layoutInflater.inflate(R.layout.fragment_learning_planner_day_choose_gridview, null);
            holder = new ViewHolder();
            holder.day = weekListView.findViewById(R.id.learningPlannerDayChooseDayTextView);
            holder.date = weekListView.findViewById(R.id.learningPlannerDayChooseDateTextView);
            holder.year = weekListView.findViewById(R.id.learningPlannerDayChooseYearTextView);
            weekListView.setTag(holder);
        } else
            holder = (ViewHolder) weekListView.getTag();

        if (isChosenDayInList(position))
            weekListView.findViewById(R.id.learningPlannerDayChooseDateTextView).setBackgroundColor(Color.parseColor("#00343434"));

        chosenDay = this.daysOfOneWeek.get(position);
        holder.day.setText(chosenDay.getNameOfDay());
        holder.date.setText(chosenDay.getDateOfDay());
        holder.year.setText(String.valueOf(chosenDay.getYear()));

        if (isNowMarker())
            weekListView.findViewById(R.id.learningPlannerDayChooseDayTextView).setBackgroundColor(Color.parseColor("#FF9800"));
        return weekListView;
    }

    private Boolean isChosenDayInList(int position) {
        return position == chosenDayIndex;
    }

    private Boolean isNowMarker() {
        return today.getDateOfDay().equals(chosenDay.getDateOfDay()) && chosenDay.getYear().equals(today.getYear());
    }

    static class ViewHolder {
        TextView day;
        TextView date;
        TextView year;
    }
}
