package com.example.myapplication.ui.learningPlanner.Calendar;

import java.util.ArrayList;
import java.util.List;

/**
 * Week is to create a complete Week from Today or from a specific date
 */
public class Week {
    private List<Day> dayOfTheWeek = new ArrayList<>();

    public Week() {
        setThisWeek();
    }

    public Week(String date) {
        for (int count = 0; count < 7; count++) {
            Day nextDay = new Day(date);
            nextDay.setAnOtherDate(count);
            dayOfTheWeek.add(nextDay);
        }
    }


    public void setThisWeek() {
        dayOfTheWeek.clear();
        for (int count = 0; count < 7; count++) {
            Day nextDay = new Day();
            nextDay.setAnOtherDate(count);
            dayOfTheWeek.add(nextDay);
        }
    }


    public List<Day> getDaysOfTheWeek() {
        return dayOfTheWeek;
    }


    public void setDayOfTheWeek(List<Day> dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }


    public List<Day> getDaysOfTheLastWeek() {
        String firstDay = dayOfTheWeek.get(0).getCompleteDate();
        dayOfTheWeek.clear();

        for (int count = -7; count < 0; count++) {
            Day nextDay = new Day(firstDay);
            nextDay.setAnOtherDate(count);
            dayOfTheWeek.add(nextDay);
        }
        return dayOfTheWeek;
    }

    public List<Day> getDaysOfTheNextWeek() {
        String firstDay = dayOfTheWeek.get(0).getCompleteDate();
        dayOfTheWeek.clear();

        for (int count = 7; count < 14; count++) {
            Day nextDay = new Day(firstDay);
            nextDay.setAnOtherDate(count);
            dayOfTheWeek.add(nextDay);
        }
        return dayOfTheWeek;
    }
}
