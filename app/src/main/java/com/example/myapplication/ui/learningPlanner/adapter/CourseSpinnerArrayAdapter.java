package com.example.myapplication.ui.learningPlanner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.myapplication.R;
import com.example.myapplication.data.Course;

import java.util.List;

public class CourseSpinnerArrayAdapter extends ArrayAdapter {
    final List<Course> courses;
    private final LayoutInflater layoutInflater;


    public CourseSpinnerArrayAdapter(Context context, List<Course> courses) {
        super(context, 0, courses);
        this.courses = courses;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return courses.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Course getItem(int position) {
        //It is just an example
        return courses.get(position);
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        return getCustom(position, view, parent);
    }


    @Override
    public View getDropDownView(int position, View view, @NonNull ViewGroup parent) {
        return getCustom(position, view, parent);
    }


    public View getCustom(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.fragment_learning_planner_course_spinner_item, null);
            holder.courseName = view.findViewById(R.id.learningPlannerSpinnerItemTextView);
            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        String tex = courses.get(position).getCourseName();
        holder.courseName.setText(tex);
        return view;
    }

    private static class ViewHolder {
        TextView courseName;
    }
}
