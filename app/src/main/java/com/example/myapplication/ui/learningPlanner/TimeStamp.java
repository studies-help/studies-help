package com.example.myapplication.ui.learningPlanner;

import com.example.myapplication.data.EventLearningPlanner;

import java.util.List;

/**
 * Object to organize all events for a specific Time between two hours.
 */
public class TimeStamp {
    private String timeStamp;
    private List<EventLearningPlanner> events;

    public TimeStamp(String timeStamp, List<EventLearningPlanner> events) {
        this.timeStamp = timeStamp;
        this.events = events;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public List<EventLearningPlanner> getEvents() {
        return events;
    }

    public void setEvents(List<EventLearningPlanner> events) {
        this.events = events;
    }
}
