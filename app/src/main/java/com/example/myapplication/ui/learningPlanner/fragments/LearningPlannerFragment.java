package com.example.myapplication.ui.learningPlanner.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.fragments.DayChoose.DayChooseOverviewFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class LearningPlannerFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        FragmentManager fm = getParentFragmentManager();
        FloatingActionButton addButton = getActivity().findViewById(R.id.fab);
        addButton.setImageResource(R.drawable.ic_add);
        addButton.setVisibility(View.VISIBLE);
        addButton.setOnClickListener(this::onClickAddButton);
        View root = inflater.inflate(R.layout.fragment_learning_planner, container, false);
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        DayChooseOverviewFragment dayChooseOverviewFragment = new DayChooseOverviewFragment();
        fragmentTransaction.replace(R.id.learningPlannerDayChooseFragmentDayChooseOverview, dayChooseOverviewFragment);
        fragmentTransaction.commit();

        return root;
    }

    private void onClickAddButton(View view) {
        LearningPlannerEditFragment learningPlannerEditFragment = LearningPlannerEditFragment.newInstance(null, getParentFragmentManager());
        learningPlannerEditFragment.show(getParentFragmentManager(), "learningPlannerEditFragment");
    }
}