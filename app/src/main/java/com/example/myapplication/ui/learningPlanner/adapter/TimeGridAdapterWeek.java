package com.example.myapplication.ui.learningPlanner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.TimeStamp;
import com.example.myapplication.ui.learningPlanner.WeekOverview;
import com.example.myapplication.ui.learningPlanner.fragments.LearningPlannerEditFragment;
import com.example.myapplication.ui.learningPlanner.modifiedGridView.MyGridView;

import java.util.List;

/**
 * adapter to fill Grid of WeekOverViewFragment with Timestamp-Elements
 */

public class TimeGridAdapterWeek extends BaseAdapter {

    private final List<TimeStamp> timeStamps;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private final Day today = new Day();
    private final FragmentManager fragmentManager;
    private final Day firstDayOfTheWeek;

    public TimeGridAdapterWeek(Context aContext, WeekOverview weekoverview, FragmentManager fragmentManager) {
        this.context = aContext;
        this.timeStamps = weekoverview.getTimestamps();
        this.firstDayOfTheWeek = weekoverview.getDays().get(0);

        layoutInflater = LayoutInflater.from(aContext);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return timeStamps.size();
    }

    @Override
    public Object getItem(int position) {
        return timeStamps.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.d("TimeGridAdapter", "execute Position" + position);
        return position;
    }

    public View getView(int position, View weekListView, ViewGroup parent) {
        ViewHolderTime holderTime;
        TimeStamp timeStamp = timeStamps.get(position);

        if (weekListView == null) {
            weekListView = layoutInflater.inflate(R.layout.fragment_learning_planner_week_overview_timestamp, null);
            holderTime = new ViewHolderTime();

            holderTime.time = weekListView.findViewById(R.id.learningPlannerTimeStampStringTextView);
            weekListView.setTag(holderTime);

        } else
            holderTime = (ViewHolderTime) weekListView.getTag();

        if (isNow(timeStamp))
            weekListView.findViewById(R.id.learningPlannerTimeStampStringTextView).setBackgroundColor(Color.parseColor("#FF9800"));
        else
            weekListView.findViewById(R.id.learningPlannerTimeStampStringTextView).setBackgroundColor(Color.parseColor("#ACACAC"));
        holderTime.time.setText(timeStamp.getTimeStamp());

        MyGridView gridView = weekListView.findViewById(R.id.learningPlannerWeekEventGrid);

        gridView.setAdapter(new EventWeekGridAdapter(context, timeStamp));
        Log.d("TimeGridAdapterWeek", "Übergebene Events" + timeStamp.getEvents().size());
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TimeGridAdapter", "Choose event");
                if (!timeStamp.getEvents().get(position).getEventStartTime().equals("")) {
                    LearningPlannerEditFragment learningPlannerEditFragment = LearningPlannerEditFragment.newInstance(timeStamp.getEvents().get(position), fragmentManager);
                    learningPlannerEditFragment.show(fragmentManager, "learningPlannerEditFragment");
                }
            }
        });
        return weekListView;
    }


    static class ViewHolderTime {
        TextView time;
    }

    public Boolean isNow(TimeStamp timeStamp) {
        return timeStamp.getTimeStamp().equals(today.getTime()) && today.getCompleteDate().equals(firstDayOfTheWeek.getCompleteDate());
    }
}

