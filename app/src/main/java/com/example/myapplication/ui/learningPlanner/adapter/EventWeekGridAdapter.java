package com.example.myapplication.ui.learningPlanner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.data.EventLearningPlanner;
import com.example.myapplication.ui.learningPlanner.TimeStamp;

import java.util.ArrayList;
import java.util.List;

/**
 * GridApter for Grid in a Timestamp in the Grid of the WeekOverview
 */
public class EventWeekGridAdapter extends BaseAdapter {
    private final LayoutInflater layoutInflater;
    private final List<EventLearningPlanner> eventsDuringFullHour;
    private final String hourOfTimeStamp;
    private final List<EventLearningPlanner> eventEnd = new ArrayList<>();

    public EventWeekGridAdapter(Context aContext, TimeStamp timeStamp) {
        layoutInflater = LayoutInflater.from(aContext);
        eventsDuringFullHour = timeStamp.getEvents();
        hourOfTimeStamp = timeStamp.getTimeStamp().substring(0, 2);
    }

    @Override
    public int getCount() {
        return eventsDuringFullHour.size();
    }

    @Override
    public Object getItem(int position) {
        return eventsDuringFullHour.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View gridViewEventExist, ViewGroup parent) {
        ViewHolderEvent holderEvent;
        EventLearningPlanner e = eventsDuringFullHour.get(position);
        eventEnd.add(position, null);

        if (gridViewEventExist == null) {
            gridViewEventExist = layoutInflater.inflate(R.layout.fragment_learning_planner_week_overview_event_gridview, null);
            holderEvent = new ViewHolderEvent();

            holderEvent.startTimeOfEvent = gridViewEventExist.findViewById(R.id.learningPlannerWeekStartTimeOfEventTextView);
            holderEvent.endTimeOfEvent = gridViewEventExist.findViewById(R.id.learningPlannerWeekendTimeOfEventTextView);
            holderEvent.eventName = gridViewEventExist.findViewById(R.id.learningPlannerWeekEventNameTextView);

            gridViewEventExist.setTag(holderEvent);
        } else
            holderEvent = (ViewHolderEvent) gridViewEventExist.getTag();

        Log.d("Event", "matching event gefunden");
        if (!e.getEventStartTime().equals("") && (hourOfTimeStamp.equals(e.getEventStartTime().substring(0, 2)))) {
            holderEvent.startTimeOfEvent.setText(e.getEventStartTime());
            holderEvent.eventName.setText(e.getEventName());
            gridViewEventExist.findViewById(R.id.learningPlannerWeekEventMarkerLineUp).setBackgroundColor(Color.parseColor("#000000"));
            eventEnd.add(position, e);
        } else {
            holderEvent.startTimeOfEvent.setText("");
            holderEvent.eventName.setText("");
            if (position > 7)
                eventEnd.add(position, eventEnd.get(position - 7));
        }
        if (!e.getEventEndTime().equals("") && hourOfTimeStamp.equals(e.getEventEndTime().substring(0, 2))) {
            holderEvent.endTimeOfEvent.setText(e.getEventEndTime());
            gridViewEventExist.findViewById(R.id.learningPlannerWeekEventMarkerLineDown).setBackgroundColor(Color.parseColor("#000000"));
            eventEnd.add(position, e);
        } else
            holderEvent.endTimeOfEvent.setText("");

        if (((position > 7 && eventEnd.get(position - 7) != null) || eventEnd.get(position) != null) || !e.getEventStartTime().equals(""))
            gridViewEventExist.setBackgroundColor(Color.parseColor("#638AE3"));
        return gridViewEventExist;
    }


    static class ViewHolderEvent {
        TextView startTimeOfEvent;
        TextView endTimeOfEvent;
        TextView eventName;
    }
}
