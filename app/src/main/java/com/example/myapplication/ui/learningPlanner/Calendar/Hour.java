package com.example.myapplication.ui.learningPlanner.Calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/** Just to get only Hour oder full Hour as Time
 *
  */

public class Hour {
    private final DateFormat dateFormatFullHour = new SimpleDateFormat("HH:00", Locale.GERMANY);
    private final DateFormat dateFormatHour = new SimpleDateFormat("HH", Locale.GERMANY);
    private final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));


    public Hour(Integer hour) {
        calendar.set(Calendar.HOUR_OF_DAY, hour);
    }


    public String getFullHour() {
        return dateFormatFullHour.format(calendar.getTime());
    }


    public String getOnlyHour() {
        return dateFormatHour.format(calendar.getTime());
    }
}
