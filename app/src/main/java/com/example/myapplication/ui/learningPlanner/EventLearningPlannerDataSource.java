package com.example.myapplication.ui.learningPlanner;

import android.content.Context;

import com.example.myapplication.data.AppDatabase;
import com.example.myapplication.data.Course;
import com.example.myapplication.data.DbSingleton;
import com.example.myapplication.data.EventLearningPlanner;

import java.util.List;

/**
 * Class to get Instance of the Database and aces the data.
 */

public class EventLearningPlannerDataSource implements EventLearningPlannerDataSourceInterface {
    private final AppDatabase appDatabase;

    public EventLearningPlannerDataSource(Context context) {
        this.appDatabase = DbSingleton.instance(context).getAppDatabase();
    }


    @Override
    public void updateData(EventLearningPlanner events) {
        appDatabase.eventLearningPlannerDao().updateAll(events);
    }

    @Override
    public void addData(EventLearningPlanner event) {
        appDatabase.eventLearningPlannerDao().insertAll(event);

    }

    @Override
    public List<EventLearningPlanner> getEventDuringHour(String date, String time) {
        return appDatabase.eventLearningPlannerDao().loadEventsDuringHour(date, time);
    }

    @Override
    public List<EventLearningPlanner> getEventStartAtHour(String date, String time) {
        return appDatabase.eventLearningPlannerDao().loadEventsStartAtHour(date, time);
    }

    @Override
    public List<EventLearningPlanner> getEventAtDate(String date) {
        return appDatabase.eventLearningPlannerDao().loadEvents(date);
    }

    @Override
    public List<EventLearningPlanner> getEventDuringTimePeriod(String date, String startTime, String endTime) {
        return appDatabase.eventLearningPlannerDao().loadEventsDuringTimePeriod(date, startTime, endTime);
    }

    @Override
    public void deleteEvent(EventLearningPlanner event) {
        appDatabase.eventLearningPlannerDao().delete(event);
    }

    @Override
    public List<Course> getCourse(Integer id) {
        return appDatabase.courseDao().getCourse(id);
    }

    /**
     * add course "Kein" as  Value for Choose No Course in the SpinnerAdapter
     * @return course
     */
    @Override
    public List<Course> getCoursesForCourseChoice() {
        List<Course> courses = appDatabase.courseDao().getAll();
        courses.add(0, new Course("Kein", "", "", "", ""));
        return courses;
    }
}
