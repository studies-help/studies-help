package com.example.myapplication.ui.learningPlanner.Calendar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Day save date from Calendar for one day now, in future or in the past
 */
public class Day {

    private String nameOfDay;
    private String dateOfDay;
    private Integer year;
    final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
    private final DateFormat dateFormatDay = new SimpleDateFormat("EEE", Locale.GERMANY);
    private final DateFormat dateFormatYear = new SimpleDateFormat("yyyy", Locale.GERMANY);
    private final DateFormat dateFormatDate = new SimpleDateFormat("dd.MM", Locale.GERMANY);
    private final DateFormat dateFormatCompleteDate = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
    private final DateFormat dateFormatHour = new SimpleDateFormat("HH:00", Locale.GERMANY);


    public Day() {
        dateOfDay = dateFormatDate.format(calendar.getTime());
        year = Integer.parseInt(dateFormatYear.format(calendar.getTime()));
        nameOfDay = dateFormatDay.format(calendar.getTime());
    }


    public Day(String newDate) {
        setAnOtherDate(newDate);

        dateOfDay = dateFormatDate.format(calendar.getTime());
        year = Integer.parseInt(dateFormatYear.format(calendar.getTime()));
        nameOfDay = dateFormatDay.format(calendar.getTime());
    }

    public String getDateOfDay() {
        return dateOfDay;
    }

    public void setDateOfDay(String dateOfDay) {
        this.dateOfDay = dateOfDay;
    }

    public String getNameOfDay() {
        return nameOfDay;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setNameOfDay(String nameOfDay) {
        this.nameOfDay = nameOfDay;
    }


    /**
     * set Calendar to x days from Today
     */

    public void setAnOtherDate(Integer daysFromToday) {
        calendar.add(Calendar.DAY_OF_YEAR, daysFromToday);
        dateOfDay = dateFormatDate.format(calendar.getTime());
        year = Integer.parseInt(dateFormatYear.format(calendar.getTime()));
        nameOfDay = dateFormatDay.format(calendar.getTime());
    }

    public void setAnOtherDate(String date) {
        Integer year = Integer.parseInt(date.substring(6, 10));
        Integer month = Integer.parseInt(date.substring(3, 5));
        Integer day = Integer.parseInt(date.substring(0, 2));

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month - 1);
    }


    public String getCompleteDate() {
        return dateFormatCompleteDate.format(calendar.getTime());
    }


    public String getTime() {
        return dateFormatHour.format(calendar.getTime());
    }


    public String getFirstDayOfTheWeek(String newDate) throws ParseException {
        String today = dateFormatCompleteDate.format(calendar.getTime());

        long diff = getDayDifference(today, newDate);
        Integer moduloDiff = (int) diff % 7;
        if (diff > 7 && diff % 7 == 0)
            return newDate;
        else if (diff > 7 && moduloDiff != 0) {
            setAnOtherDate(newDate);
            setAnOtherDate(-moduloDiff);
            return getCompleteDate();
        } else if (diff < 0 && moduloDiff == 0)
            return newDate;
        else if (diff < 0 && moduloDiff != 0) {
            setAnOtherDate(newDate);
            setAnOtherDate(-(moduloDiff + 7));
            return getCompleteDate();
        } else
            return getCompleteDate();
    }

    public long getDayDifference(String firstDate, String secondDate) throws ParseException {
        Date date = dateFormatCompleteDate.parse(firstDate);
        Date dateSecond = dateFormatCompleteDate.parse(secondDate);
        return TimeUnit.DAYS.convert(dateSecond.getTime() - date.getTime(), TimeUnit.MILLISECONDS);
    }
}
