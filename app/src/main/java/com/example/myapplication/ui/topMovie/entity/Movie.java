package com.example.myapplication.ui.topMovie.entity;


public class Movie {
    String imdbId;
    String title;
    String year;
    String poster;
    String description;


    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {

        this.imdbId = imdbId.split("/")[2];
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "imdb_id='" + imdbId + '\'' +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", poster='" + poster + '\'' +
                ", discription='" + description + '\'' +
                '}';
    }

    public Movie() {
    }
}
