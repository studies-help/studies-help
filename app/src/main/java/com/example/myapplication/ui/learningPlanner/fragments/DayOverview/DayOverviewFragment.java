package com.example.myapplication.ui.learningPlanner.fragments.DayOverview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.fragment.app.DialogFragment;

import com.example.myapplication.R;
import com.example.myapplication.ui.learningPlanner.DayOverview;
import com.example.myapplication.ui.learningPlanner.adapter.TimeGridAdapterDay;


public class DayOverviewFragment extends DialogFragment {

    private DayOverview dayoverview;
    private String date;

    public DayOverviewFragment(String date) {
        this.date = date;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            date = getArguments().getString("date");
        dayoverview = new DayOverview(getActivity(), date);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_learning_planner_day_overview, container, false);
        final GridView gridView = root.findViewById(R.id.learningPlannerDayTimeGrid);
        gridView.setAdapter(new TimeGridAdapterDay(getActivity(), dayoverview, getParentFragmentManager()));
        // Inflate the layout for this fragment
        return root;
    }
}