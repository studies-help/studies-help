package com.example.myapplication.ui.learningCards;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.data.AppDatabase;
import com.example.myapplication.data.DbSingleton;
import com.example.myapplication.data.LearningCard;

import org.commonmark.node.Node;

import java.lang.ref.WeakReference;

import io.noties.markwon.Markwon;


/**
 * Activity where learning cards can be created or edited
 */

public class LearningCardsEditorActivity extends AppCompatActivity {
    private long learningCardId;
    private EditText learningCardContentEditText;
    private EditText learningCardTitleEditText;
    private TextView markdownTextView;
    private AppDatabase appDatabase;
    private String learningCardContentFormat;
    private int learningCardContentAlignment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learningcards_editor);
        this.appDatabase = DbSingleton.instance(this).getAppDatabase();
        learningCardContentEditText = findViewById(R.id.learningCardsContentEditText);
        learningCardTitleEditText = findViewById(R.id.learningCardTitleEditText);
        markdownTextView = findViewById(R.id.markdownTextView);
        learningCardId = getIntent().getExtras().getInt("learningCardID");
        learningCardContentAlignment = getIntent().getExtras().getInt("learningCardContentAlignment");
        learningCardTitleEditText.setText(getIntent().getExtras().getString("learningCardTitle"));
        learningCardContentFormat = getIntent().getExtras().getString("learningCardContent");
        String learningCardContent = learningCardContentFormat;
        final Markwon markwon = Markwon.create(this);
        final Node node = markwon.parse(learningCardContent);
        markwon.render(node);
        markwon.setMarkdown(markdownTextView, learningCardContent);
        findViewById(R.id.closeButton).setOnClickListener(this::onClickCloseButton);
        findViewById(R.id.saveButton).setOnClickListener(this::onClickSaveButton);
        findViewById(R.id.leftAlignmentButton).setOnClickListener(this::onClickAlignmentButton);
        findViewById(R.id.centerAlignmentButton).setOnClickListener(this::onClickAlignmentButton);
        findViewById(R.id.rightAlignmentButton).setOnClickListener(this::onClickAlignmentButton);
        findViewById(R.id.toggleView).setOnClickListener(this::onClickToggleView);
        findViewById(R.id.toggleView).setTag(true);
        ((ImageView) findViewById(R.id.toggleView)).setImageResource(R.drawable.ic_edit);
        (findViewById(R.id.textEditor)).setVisibility(View.GONE);
        markdownTextView.setVisibility(View.VISIBLE);
    }


    /**
     * switches editor view and view with parsed markdown
     *
     * @param view current view
     */

    private void onClickToggleView(View view) {
        ImageView toggleView = findViewById(R.id.toggleView);
        boolean tag = (boolean) toggleView.getTag();
        if (tag) {
            findViewById(R.id.textEditor).setVisibility(View.VISIBLE);
            markdownTextView.setVisibility(View.GONE);
            toggleView.setImageResource(R.drawable.ic_watch);
            toggleView.setTag(false);
            learningCardContentEditText.setText(learningCardContentFormat);
            Log.d("LearningCardsEditor", "edit mode");
        } else {
            learningCardContentFormat = learningCardContentEditText.getText().toString();
            (findViewById(R.id.textEditor)).setVisibility(View.GONE);
            markdownTextView.setVisibility(View.VISIBLE);
            toggleView.setImageResource(R.drawable.ic_edit);
            toggleView.setTag(true);
            String learningCardContent = learningCardContentFormat;
            final Markwon markwon = Markwon.create(this);
            final Node node = markwon.parse(learningCardContent);
            markwon.render(node);
            markwon.setMarkdown(markdownTextView, learningCardContent);
            Log.d("LearningCardsEditor", "preview mode");
        }
    }


    private void onClickAlignmentButton(View view) {
        if (view.equals(findViewById(R.id.leftAlignmentButton))) {
            learningCardContentEditText.setGravity(Gravity.START);
            learningCardContentAlignment = Gravity.START;
        } else if (view.equals(findViewById(R.id.centerAlignmentButton))) {
            learningCardContentEditText.setGravity(Gravity.CENTER_HORIZONTAL);
            learningCardContentAlignment = Gravity.CENTER_HORIZONTAL;
        } else if (view.equals(findViewById(R.id.rightAlignmentButton))) {
            learningCardContentEditText.setGravity(Gravity.END);
            learningCardContentAlignment = Gravity.END;
        }
    }


    /**
     * saves the actual learning card
     */

    private void save() {
        learningCardContentFormat = learningCardContentEditText.getText().toString();
        Log.d("Test", learningCardContentFormat);
        LearningCard learningCard;
        if (learningCardId != Long.MIN_VALUE) {
            learningCard = new LearningCard(learningCardId, learningCardContentEditText.getText().toString(),
                    learningCardTitleEditText.getText().toString(), learningCardContentAlignment);
            appDatabase.learningCardDao().onUpdate(learningCard);
            setResult(learningCard, 2);
        } else {
            learningCard = new LearningCard(learningCardId, learningCardContentEditText.getText().toString(),
                    learningCardTitleEditText.getText().toString(), learningCardContentAlignment);
            appDatabase.learningCardDao().insert(learningCard);
            setResult(learningCard, 3);
        }
        // create worker thread to insert data into database
        new InsertTask(LearningCardsEditorActivity.this, learningCard).doInBackground();
    }


    private void onClickCloseButton(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Speichern?")
                .setMessage("Soll die Lernkarte gespeichert werden?\nDas Löschen der Lernkarte " +
                        "kann nicht rückgängig gemacht werden")
                .setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        save();
                        onBackPressed();
                    }
                }).setNegativeButton("Löschen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        })
                .show();
        Log.d("LearningCardsEditor", "close editor");
    }


    private void onClickSaveButton(View view) {
        save();
    }


    private void setResult(LearningCard learningCard, int flag) {
        setResult(flag, new Intent().putExtra("note", learningCard.toString()));
        finish();
    }


    private static class InsertTask {
        final private WeakReference<LearningCardsEditorActivity> activityReference;
        final private LearningCard learningCard;

        // only retain a weak reference to the activity
        InsertTask(LearningCardsEditorActivity context, LearningCard learningCard) {
            activityReference = new WeakReference<>(context);
            this.learningCard = learningCard;
        }

        // doInBackground methods runs on a worker thread
        protected void doInBackground() {
            // retrieve auto incremented learning card id
            long j = activityReference.get().appDatabase.learningCardDao().insert(learningCard);
            learningCard.setLearningCardID(j);
        }
    }
}
