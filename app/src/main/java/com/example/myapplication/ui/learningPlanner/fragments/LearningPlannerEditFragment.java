package com.example.myapplication.ui.learningPlanner.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.data.Course;
import com.example.myapplication.data.EventLearningPlanner;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.EventLearningPlannerDataSource;
import com.example.myapplication.ui.learningPlanner.adapter.CourseSpinnerArrayAdapter;
import com.example.myapplication.ui.learningPlanner.fragments.DayChoose.DayChooseOverviewFragment;
import com.example.myapplication.ui.learningPlanner.fragments.DayOverview.DayOverviewFragment;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;


/**
 * Fragment for Frame for Editing, add, delete Event
 */

public class LearningPlannerEditFragment extends DialogFragment {
    private final EventLearningPlannerDataSource dataSource;
    private Boolean timeIsOkay = false;
    private Boolean dateIsOkay = false;
    private String dateOfEvent;
    private final FragmentManager fragmentManager;
    private Integer courseID;
    private EventLearningPlanner event;
    private final Integer oldEventID;


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


    public LearningPlannerEditFragment(EventLearningPlanner event, FragmentManager fragmentManager) {
        dataSource = new EventLearningPlannerDataSource(getContext());
        this.event = event;
        if (event != null)
            this.dateOfEvent = event.getEventDate();

        oldEventID = event == null ? -1 : event.getEventID();
        this.fragmentManager = fragmentManager;
    }


    public static LearningPlannerEditFragment newInstance(EventLearningPlanner event, FragmentManager fragmentManager) {
        return new LearningPlannerEditFragment(event, fragmentManager);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_learning_planner_edit, container, false);
        final EditText kindOfEventEditText = root.findViewById(R.id.learningPlannerKindOfEventEditText);
        final EditText placeOfEventEditText = root.findViewById(R.id.learningPlannerPlaceOfEventEditText);
        final EditText learningGroupEditText = root.findViewById(R.id.learningPlannerLearnGroupEditText);
        final EditText dateOfEventEditText = root.findViewById(R.id.learningPlannerEditTextDate);
        final Spinner coursesSpinner = root.findViewById(R.id.learningPlannerSpinner);
        final EditText timeEditText = root.findViewById(R.id.learningPlannerTimeEditText);
        final EditText additionalInformationEditText = root.findViewById(R.id.learningPlannerAdditionalInformationEditText);
        final ImageButton submitButton = root.findViewById(R.id.learningPlannerSubmitImageButton);
        final ImageButton deleteButton = root.findViewById(R.id.learningPlannerDeleteButton);
        final Button editButton = root.findViewById(R.id.learningPlannerSetEditAvailable);
        final TextView submitErrorTextView = root.findViewById(R.id.learningPlannerSubmitErrorTextView);

        if (event != null) {
            Log.d("EditFragment", "old event");
            kindOfEventEditText.setText(event.getEventName());
            kindOfEventEditText.setEnabled(false);
            placeOfEventEditText.setText(event.getEventPlace());
            placeOfEventEditText.setEnabled(false);
            learningGroupEditText.setText(event.getEventInvolved());
            learningGroupEditText.setEnabled(false);
            dateOfEventEditText.setText(event.getEventDate());
            dateOfEventEditText.setEnabled(false);
            timeEditText.setText(event.getEventCompleteTime());
            timeEditText.setEnabled(false);
            coursesSpinner.setEnabled(false);
            coursesSpinner.setAdapter(new CourseSpinnerArrayAdapter(getContext(), new EventLearningPlannerDataSource(getContext()).getCourse(event.getCourseID())));
            additionalInformationEditText.setText(event.getEventDescription());
            additionalInformationEditText.setEnabled(false);
            editButton.setVisibility(View.VISIBLE);
            submitButton.setVisibility(View.INVISIBLE);
        } else {
            List<Course> courses = new EventLearningPlannerDataSource(getContext()).getCoursesForCourseChoice();
            CourseSpinnerArrayAdapter dataAdapter = new CourseSpinnerArrayAdapter(getContext(), courses);
            coursesSpinner.setAdapter(dataAdapter);
            coursesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                    courseID = courses.get(pos).getCourseID();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
        TextWatcher dateChangeListener = new TextWatcher() {
            private String current = "";
            @SuppressWarnings("FieldCanBeLocal")
            private final String ddmmyyyy = "TTMMJJJJ";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                    String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8) {
                        clean = clean + ddmmyyyy.substring(clean.length());
                        dateIsOkay = false;
                    } else {
                        int day = Integer.parseInt(clean.substring(0, 2));
                        int mon = Integer.parseInt(clean.substring(2, 4));
                        int year = Integer.parseInt(clean.substring(4, 8));

                        mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                        cal.set(Calendar.DAY_OF_MONTH, 1);

                        cal.set(Calendar.MONTH, mon - 1);
                        year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                        cal.set(Calendar.YEAR, year);
                        day = (day > cal.getActualMaximum(Calendar.DAY_OF_MONTH)) ? cal.getActualMaximum(Calendar.DATE) : day;
                        clean = String.format("%02d%02d%02d", day, mon, year);
                    }

                    clean = String.format("%s.%s.%s", clean.substring(0, 2),
                            clean.substring(2, 4),
                            clean.substring(4, 8));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    dateOfEventEditText.setText(current);
                    dateOfEventEditText.setSelection(sel < current.length() ? sel : current.length());
                    dateIsOkay = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        TextWatcher timeChangeListener = new TextWatcher() {
            private String current = "";
            @SuppressWarnings("FieldCanBeLocal")
            private final String format = "HH:MM-HH:MM";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                    String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8) {
                        clean = clean + format.substring(clean.length());
                        timeIsOkay = false;
                    } else {
                        int hoursStart = Integer.parseInt(clean.substring(0, 2));
                        int minutesStart = Integer.parseInt(clean.substring(2, 4));
                        int hoursEnd = Integer.parseInt(clean.substring(4, 6));
                        int minutesEnd = Integer.parseInt(clean.substring(6, 8));

                        minutesStart = minutesStart < 0 ? 0 : minutesStart > 59 ? 59 : minutesStart;

                        hoursStart = (hoursStart < 0) ? 0 : hoursStart > 23 ? 23 : hoursStart;

                        hoursEnd = (hoursEnd < 0) ? 0 : hoursEnd > 23 ? 23 : hoursStart > hoursEnd ? hoursStart : hoursEnd;
                        minutesEnd = minutesEnd > 9 ? minutesEnd : minutesEnd * 10;
                        minutesEnd = minutesEnd < 1 ? 00 : minutesEnd > 59 ? 59 : hoursEnd != hoursStart ? minutesEnd : minutesStart > minutesEnd ? minutesStart : minutesEnd;

                        clean = String.format("%02d%02d%02d%02d", hoursStart, minutesStart, hoursEnd, minutesEnd);
                        timeIsOkay = true;
                    }

                    clean = String.format("%s:%s-%s:%s", clean.substring(0, 2),
                            clean.substring(2, 4), clean.substring(4, 6), clean.substring(6, 8)
                    );

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    timeEditText.setText(current);
                    timeEditText.setSelection(sel < current.length() ? sel : current.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        dateOfEventEditText.addTextChangedListener(dateChangeListener);
        timeEditText.addTextChangedListener(timeChangeListener);

        final ImageButton exitButton = root.findViewById(R.id.learningPlannerExitButton);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String kindOfEvent = kindOfEventEditText.getText().toString();
                String placeOfEvent = placeOfEventEditText.getText().toString();
                String learningGroup = learningGroupEditText.getText().toString();
                dateOfEvent = dateOfEventEditText.getText().toString();
                String time = timeEditText.getText().toString();
                String additionalInformation = additionalInformationEditText.getText().toString();

                if (dateIsOkay && timeIsOkay) {
                    if (event == null) {
                        Log.d("LearningPlannerEditFragment", "EndTime ist " + time.substring(6, 9));
                        event = new EventLearningPlanner(kindOfEvent, time.substring(0, 5), time.substring(6, 11), dateOfEvent, additionalInformation, learningGroup, placeOfEvent);
                        event.setCourseID(courseID == 0 ? null : courseID);

                        if (checkEventInTimeAlreadyExist(event)) {
                            Log.d("LearningPlannerEditFragment", "EventTime is occupied");
                            submitErrorTextView.setVisibility(View.VISIBLE);
                            submitErrorTextView.setText("Das Zeitfenster wurde (teilweise) bereits verplant");
                            event= null;
                        } else {
                            dataSource.addData(event);
                            refreshOverview();
                            Log.d("LearningPlannerEditFragment", "EventTime is not occupied");
                            dismiss();
                        }
                    } else {
                        event.setEventDate(dateOfEvent);
                        event.setEventDescription(additionalInformation);
                        event.setEventStartTime(time.substring(0, 5));
                        event.setEventEndTime(time.substring(6, 11));
                        event.setEventPlace(placeOfEvent);
                        event.setEventInvolved(learningGroup);
                        event.setEventName(kindOfEvent);

                        if (event != null && checkEventInTimeAlreadyExist(event)) {
                            Log.d("LearningPlannerEditFragment", "Update Event EventTime is occupied");
                            submitErrorTextView.setVisibility(View.VISIBLE);
                            submitErrorTextView.setText("Das Zeitfenster wurde (teilweise) bereits verplant");
                        } else {
                            dataSource.updateData(event);
                            refreshOverview();

                            Log.d("LearningPlannerEditFragment", "update Event");
                            dismiss();
                        }
                    }
                }
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(requireActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Löschen?")
                        .setMessage("Sind Sie sicher, dass der Eintrag gelöscht werden soll?\n" +
                                "Dieser Vorgang kann nicht rückgängig gemacht werden!")
                        .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dataSource.deleteEvent(event);
                                refreshOverview();
                                Log.d("Database", "delete event");
                                dismiss();
                            }
                        }).setNegativeButton("Nicht löschen", null)
                        .show();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setVisibility(View.INVISIBLE);
                submitButton.setVisibility(View.VISIBLE);
                deleteButton.setVisibility(View.VISIBLE);
                kindOfEventEditText.setEnabled(true);
                placeOfEventEditText.setEnabled(true);
                learningGroupEditText.setEnabled(true);
                dateOfEventEditText.setEnabled(true);
                timeEditText.setEnabled(true);
                additionalInformationEditText.setEnabled(true);
                dateIsOkay = true;
                timeIsOkay = true;
                coursesSpinner.setEnabled(true);

                List<Course> courses = new EventLearningPlannerDataSource(getContext()).getCoursesForCourseChoice();
                CourseSpinnerArrayAdapter dataAdapter = new CourseSpinnerArrayAdapter(getContext(), courses);

                coursesSpinner.setAdapter(dataAdapter);
                if (event.getCourseID() != null) {
                    List<Course> course = new EventLearningPlannerDataSource(getContext()).getCourse(event.getCourseID());
                    coursesSpinner.setSelection(findIndex(courses, course.get(0)));
                } else
                    coursesSpinner.setSelection(courses.indexOf(new Course("Kein", "", "", "", "")));

                coursesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (!courses.get(i).getCourseName().equals("Kein"))
                            event.setCourseID(courses.get(i).getCourseID());
                        else
                            event.setCourseID(null);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        if (event.getCourseID() != null)
                            coursesSpinner.setSelection(courses.indexOf(new EventLearningPlannerDataSource(getContext()).getCourse(event.getCourseID())));
                        else
                            coursesSpinner.setSelection(courses.indexOf(new Course("Kein", "", "", "", "")));
                    }
                });
            }
        });
        return root;
    }

    public Boolean checkEventInTimeAlreadyExist(EventLearningPlanner event) {
        List<EventLearningPlanner> events;
        String starTime = event.getEventStartTime();
        String endTime = event.getEventEndTime();

        events = dataSource.getEventDuringTimePeriod(dateOfEvent, starTime, endTime);
        for (EventLearningPlanner e : events) {
            if (!(oldEventID == e.getEventID()))
                return true;
        }
        return false;
    }


    public void refreshOverview() {
        try {
            String newDate = new Day().getFirstDayOfTheWeek(dateOfEvent);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.learningPlannerDayChooseFragmentPeriodSwitch, new DayOverviewFragment(dateOfEvent));
            fragmentTransaction.replace(R.id.learningPlannerDayChooseFragmentDayChooseOverview, new DayChooseOverviewFragment(newDate, dateOfEvent));
            fragmentTransaction.commit();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Integer findIndex(List<Course> courses, Course course) {
        Integer index = 0;
        for (Course e : courses) {
            if (e.getCourseID() == course.getCourseID())
                return index;
            index++;
        }
        return -1;
    }
}