package com.example.myapplication.ui.courses;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.data.AppDatabase;
import com.example.myapplication.data.Course;
import com.example.myapplication.data.DbSingleton;

import java.util.List;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder> {
    //Initialize variable
    private final List<Course> dataList;
    private final Activity context;
    private AppDatabase database;

    //Create constructor
    public CourseAdapter(Activity context,List<Course> dataList) {
        this.context = context;
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_courses_list_row_main_course,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Course data = dataList.get(position);
        database = DbSingleton.instance(context).getAppDatabase();
        holder.textView.setText(data.getCourseName());

        holder.btView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Course d = dataList.get(holder.getAdapterPosition());
                String sText = d.getCourseName();
                String lText = d.getLecturer();
                String crText = d.getCourseRoom();
                String cdText = d.getContactData();
                String fsText = d.getFellowStudents();

                Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.fragment_courses_dialog_view_course);
                int width = WindowManager.LayoutParams.MATCH_PARENT;
                int height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setLayout(width, height);
                dialog.show();

                Button btClose = dialog.findViewById(R.id.courseCloseButton);
                TextView tvCourseName = dialog.findViewById(R.id.courseCourseNameTextView);
                TextView tvLecturer = dialog.findViewById(R.id.courseLecturerTextView);
                TextView tvCourseRoom = dialog.findViewById(R.id.courseCourseRoomTextView);
                TextView tvContactData = dialog.findViewById(R.id.courseContactDataTextView);
                TextView tvFellowStudents = dialog.findViewById(R.id.courseFellowStudentsTextView);

                tvCourseName.setText("Kursname: " + sText);
                tvLecturer.setText("Dozent: " + lText);
                tvCourseRoom.setText("Kursraum: " + crText);
                tvContactData.setText("Kontaktdaten: " + cdText);
                tvFellowStudents.setText("Kommilitonen: " + fsText);

                btClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        holder.btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Course d = dataList.get(holder.getAdapterPosition());
                int sID = d.getCourseID();
                String sText = d.getCourseName();
                String lText = d.getLecturer();
                String crText = d.getCourseRoom();
                String cdText = d.getContactData();
                String fsText = d.getFellowStudents();


                Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.fragment_courses_dialog_update_course);
                int width = WindowManager.LayoutParams.MATCH_PARENT;
                int height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setLayout(width, height);
                dialog.show();

                EditText editText = dialog.findViewById(R.id.courseUpdateCourseNameEditText);
                EditText etLecturer = dialog.findViewById(R.id.courseUpdateLecturerEditText);
                EditText etCourseRoom = dialog.findViewById(R.id.courseUpdateCourseRoomEditText);
                EditText etContactData = dialog.findViewById(R.id.courseUpdateContactDataEditText);
                EditText etFellowStudents = dialog.findViewById(R.id.courseUpdateFellowStudentsEditText);
                Button btUpdate = dialog.findViewById(R.id.courseUpdateButton);
                Button btUpdateAbort = dialog.findViewById(R.id.courseUpdateAbortButton);

                editText.setText(sText);
                etLecturer.setText(lText);
                etCourseRoom.setText(crText);
                etContactData.setText(cdText);
                etFellowStudents.setText(fsText);

                btUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String uText = editText.getText().toString().trim();
                        String ulText = etLecturer.getText().toString().trim();
                        String ucrText = etCourseRoom.getText().toString().trim();
                        String ucdText = etContactData.getText().toString().trim();
                        String ufsText = etFellowStudents.getText().toString().trim();
                        if (!uText.equals("")) {
                            database.courseDao().updateCourseName(sID,uText);
                            database.courseDao().updateLecturer(sID,ulText);
                            database.courseDao().updateCourseRoom(sID,ucrText);
                            database.courseDao().updateContactData(sID,ucdText);
                            database.courseDao().updateFellowStudents(sID,ufsText);
                            dataList.clear();
                            dataList.addAll(database.courseDao().getAll());
                            notifyDataSetChanged();
                            dialog.dismiss();
                        }
                        else {
                            new AlertDialog.Builder(context)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Fehler")
                                    .setMessage("Bitte fügen Sie einen Kursnamen ein.")
                                    .setPositiveButton("Ok", null)
                                    .setNegativeButton("Entwurf löschen", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    }
                });

                btUpdateAbort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        holder.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Löschen?")
                        .setMessage("Sind Sie sicher, dass die Kursdaten gelöscht werden sollen?\n" +
                                "Dieser Vorgang kann nicht rückgängig gemacht werden!")
                        .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Course d = dataList.get(holder.getAdapterPosition());
                                database.courseDao().delete(d);
                                database.courseDao().deleteDB(d.getCourseID());
                                int position = holder.getAdapterPosition();
                                dataList.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,dataList.size());
                            }
                        }).setNegativeButton("Nicht löschen", null)
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;
        private final ImageView btEdit;
        private final ImageView btDelete;
        private final ImageView btView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.courseTitleTextView);
            btEdit = itemView.findViewById(R.id.courseEditImageView);
            btDelete = itemView.findViewById(R.id.courseDeleteImageView);
            btView = itemView.findViewById(R.id.courseViewImageView);
        }
    }
}
