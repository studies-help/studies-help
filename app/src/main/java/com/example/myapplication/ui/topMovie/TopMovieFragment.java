package com.example.myapplication.ui.topMovie;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.ui.topMovie.entity.CheckNetwork;
import com.example.myapplication.ui.topMovie.entity.Movie;
import com.example.myapplication.ui.topMovie.entity.Variables;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.commonmark.node.Node;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.noties.markwon.Markwon;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * displays the 5 most popular movies of the world
 * by clicking on the film title, additional information such as description, release date and movie poster will be unfolded
 */

public class TopMovieFragment extends Fragment {

    private TextView movieErrorText;
    private ImageButton reloadButton;
    private LinearLayout topMoviesLayout;
    private ArrayList<String> moviePoster;

    private TextView topMovieTitle1;
    private ImageView topMovieImageView1;
    private LinearLayout topMovieLayout1;
    private TextView topMovieDescription1;
    private TextView topMovieYear1;
    private ImageView topMoviePoster1;

    private TextView topMovieTitle2;
    private ImageView topMovieImageView2;
    private LinearLayout topMovieLayout2;
    private TextView topMovieDescription2;
    private TextView topMovieYear2;
    private ImageView topMoviePoster2;

    private TextView topMovieTitle3;
    private ImageView topMovieImageView3;
    private LinearLayout topMovieLayout3;
    private TextView topMovieDescription3;
    private TextView topMovieYear3;
    private ImageView topMoviePoster3;

    private TextView topMovieTitle4;
    private ImageView topMovieImageView4;
    private LinearLayout topMovieLayout4;
    private TextView topMovieDescription4;
    private TextView topMovieYear4;
    private ImageView topMoviePoster4;

    private TextView topMovieTitle5;
    private ImageView topMovieImageView5;
    private LinearLayout topMovieLayout5;
    private TextView topMovieDescription5;
    private TextView topMovieYear5;
    private ImageView topMoviePoster5;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_top_movie, container, false);
        movieErrorText = root.findViewById(R.id.movieErrorTextField);
        reloadButton = root.findViewById(R.id.movieReloadButton);
        if(getActivity()== null) return root;
        FloatingActionButton addButton = getActivity().findViewById(R.id.fab);
        addButton.setVisibility(View.GONE);
        topMoviesLayout = root.findViewById(R.id.topMoviesLinearLayout);
        CheckNetwork checkNetwork = new CheckNetwork(getContext());
        checkNetwork.registerNetworkCallback();

        topMovieTitle1 = root.findViewById(R.id.topMovieTitle1);
        topMovieTitle1.setOnClickListener(this::onClickTopMovieTitle1);
        topMovieImageView1 = root.findViewById(R.id.topMovieImageView1);
        topMovieImageView1.setOnClickListener(this::onClickTopMovieTitle1);
        topMovieLayout1 = root.findViewById(R.id.topMovieLinearLayout1);
        topMovieDescription1 = root.findViewById(R.id.topMovieDescription1);
        topMovieYear1 = root.findViewById(R.id.topMovieYear1);
        topMoviePoster1 = root.findViewById(R.id.topMoviePoster1);

        topMovieTitle2 = root.findViewById(R.id.topMovieTitle2);
        topMovieTitle2.setOnClickListener(this::onClickTopMovieTitle2);
        topMovieImageView2 = root.findViewById(R.id.topMovieImageView2);
        topMovieImageView2.setOnClickListener(this::onClickTopMovieTitle2);
        topMovieLayout2 = root.findViewById(R.id.topMovieLinearLayout2);
        topMovieDescription2 = root.findViewById(R.id.topMovieDescription2);
        topMovieYear2 = root.findViewById(R.id.topMovieYear2);
        topMoviePoster2 = root.findViewById(R.id.topMoviePoster2);

        topMovieTitle3 = root.findViewById(R.id.topMovieTitle3);
        topMovieTitle3.setOnClickListener(this::onClickTopMovieTitle3);
        topMovieImageView3 = root.findViewById(R.id.topMovieImageView3);
        topMovieImageView3.setOnClickListener(this::onClickTopMovieTitle3);
        topMovieLayout3 = root.findViewById(R.id.topMovieLinearLayout3);
        topMovieDescription3 = root.findViewById(R.id.topMovieDescription3);
        topMovieYear3 = root.findViewById(R.id.topMovieYear3);
        topMoviePoster3 = root.findViewById(R.id.topMoviePoster3);

        topMovieTitle4 = root.findViewById(R.id.topMovieTitle4);
        topMovieTitle4.setOnClickListener(this::onClickTopMovieTitle4);
        topMovieImageView4 = root.findViewById(R.id.topMovieImageView4);
        topMovieImageView4.setOnClickListener(this::onClickTopMovieTitle4);
        topMovieLayout4 = root.findViewById(R.id.topMovieLinearLayout4);
        topMovieDescription4 = root.findViewById(R.id.topMovieDescription4);
        topMovieYear4 = root.findViewById(R.id.topMovieYear4);
        topMoviePoster4 = root.findViewById(R.id.topMoviePoster4);

        topMovieTitle5 = root.findViewById(R.id.topMovieTitle5);
        topMovieTitle5.setOnClickListener(this::onClickTopMovieTitle5);
        topMovieImageView5 = root.findViewById(R.id.topMovieImageView5);
        topMovieImageView5.setOnClickListener(this::onClickTopMovieTitle5);
        topMovieLayout5 = root.findViewById(R.id.topMovieLinearLayout5);
        topMovieDescription5 = root.findViewById(R.id.topMovieDescription5);
        topMovieYear5 = root.findViewById(R.id.topMovieYear5);
        topMoviePoster5 = root.findViewById(R.id.topMoviePoster5);

        topMovieTitle1.setTag(false);
        topMovieTitle2.setTag(false);
        topMovieTitle3.setTag(false);
        topMovieTitle4.setTag(false);
        topMovieTitle5.setTag(false);

        topMovieImageView1.setTag(false);
        topMovieImageView2.setTag(false);
        topMovieImageView3.setTag(false);
        topMovieImageView4.setTag(false);
        topMovieImageView5.setTag(false);

        new GetData().execute();
        return root;
    }


    private void onClickTopMovieTitle1(View view) {
        boolean tagTitle = (boolean) topMovieTitle1.getTag();
        boolean tagButton = (boolean) topMovieImageView1.getTag();
        if (tagTitle || tagButton) {
            topMovieLayout1.setVisibility(View.GONE);
            topMovieTitle1.setTag(false);
            topMovieImageView1.setTag(false);
            topMovieImageView1.setImageResource(R.drawable.ic_top_movie_arrow_down);
        } else {
            topMovieLayout1.setVisibility(View.VISIBLE);
            topMovieTitle1.setTag(true);
            topMovieImageView1.setTag(true);
            topMovieImageView1.setImageResource(R.drawable.ic_top_movie_arrow_up);
        }
    }


    private void onClickTopMovieTitle2(View view) {
        boolean tagTitle = (boolean) topMovieTitle2.getTag();
        boolean tagButton = (boolean) topMovieImageView2.getTag();
        if (tagTitle || tagButton) {
            topMovieLayout2.setVisibility(View.GONE);
            topMovieTitle2.setTag(false);
            topMovieImageView2.setTag(false);
            topMovieImageView2.setImageResource(R.drawable.ic_top_movie_arrow_down);
        } else {
            topMovieLayout2.setVisibility(View.VISIBLE);
            topMovieTitle2.setTag(true);
            topMovieImageView2.setTag(true);
            topMovieImageView2.setImageResource(R.drawable.ic_top_movie_arrow_up);
        }
    }


    private void onClickTopMovieTitle3(View view) {
        boolean tagTitle = (boolean) topMovieTitle3.getTag();
        boolean tagButton = (boolean) topMovieImageView3.getTag();
        if (tagTitle || tagButton) {
            topMovieLayout3.setVisibility(View.GONE);
            topMovieTitle3.setTag(false);
            topMovieImageView3.setTag(false);
            topMovieImageView3.setImageResource(R.drawable.ic_top_movie_arrow_down);
        } else {
            topMovieLayout3.setVisibility(View.VISIBLE);
            topMovieTitle3.setTag(true);
            topMovieImageView3.setTag(true);
            topMovieImageView3.setImageResource(R.drawable.ic_top_movie_arrow_up);
        }
    }


    private void onClickTopMovieTitle4(View view) {
        boolean tagTitle = (boolean) topMovieTitle4.getTag();
        boolean tagButton = (boolean) topMovieImageView4.getTag();
        if (tagTitle || tagButton) {
            topMovieLayout4.setVisibility(View.GONE);
            topMovieTitle4.setTag(false);
            topMovieImageView4.setTag(false);
            topMovieImageView4.setImageResource(R.drawable.ic_top_movie_arrow_down);
        } else {
            topMovieLayout4.setVisibility(View.VISIBLE);
            topMovieTitle4.setTag(true);
            topMovieImageView4.setTag(true);
            topMovieImageView4.setImageResource(R.drawable.ic_top_movie_arrow_up);
        }
    }


    private void onClickTopMovieTitle5(View view) {
        boolean tagTitle = (boolean) topMovieTitle5.getTag();
        boolean tagButton = (boolean) topMovieImageView5.getTag();
        if (tagTitle || tagButton) {
            topMovieLayout5.setVisibility(View.GONE);
            topMovieTitle5.setTag(false);
            topMovieImageView5.setTag(false);
            topMovieImageView5.setImageResource(R.drawable.ic_top_movie_arrow_down);
        } else {
            topMovieLayout5.setVisibility(View.VISIBLE);
            topMovieTitle5.setTag(true);
            topMovieImageView5.setTag(true);
            topMovieImageView5.setImageResource(R.drawable.ic_top_movie_arrow_up);
        }
    }


    /**
     * gets movie data asynchronously
     */

    public class GetData extends AsyncTask<Void, Void, List<Movie>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            topMoviesLayout.setVisibility(View.GONE);
            movieErrorText.setText(R.string.loadData);
            movieErrorText.setVisibility(View.VISIBLE);
            reloadButton.setVisibility(View.GONE);
        }

        @Override
        protected List<Movie> doInBackground(Void... view) {
            List<Movie> movies = new ArrayList<>();
            try {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("https://imdb8.p.rapidapi.com/title/get-most-popular-movies?homeCountry=DE&purchaseCountry=DE&currentCountry=DE")
                        .get()
                        .addHeader("x-rapidapi-key", "eebb1996bbmsh17770f2e35a3f8dp18c9d8jsn7bfad2dd8a80")
                        .addHeader("x-rapidapi-host", "imdb8.p.rapidapi.com")
                        .build();

                Response response = client.newCall(request).execute();
                String json = response.body().string();

                JSONArray jsonArray = new JSONArray(json);
                for (int c = 0; c < 5; c++) {
                    String jsonObject = jsonArray.getString(c);
                    Movie movie = new Movie();
                    movie.setImdbId(jsonObject);
                    movies.add(getAdditionalInformation(movie, client));
                }
            } catch (IOException e) {
                Log.e("HomeFragment", "Connection failed " + e.getMessage());
            } catch (JSONException e) {
                Log.e("HomeFragment", "IMDB-ID Json parsing failed: " + e.getMessage());
            }
            return movies;
        }


        protected void onPostExecute(List<Movie> movies) {
            super.onPostExecute(movies);
            movieErrorText.setVisibility(View.GONE);
            reloadButton.setVisibility(View.GONE);
            topMoviesLayout.setVisibility(View.VISIBLE);
            if (Variables.isNetworkConnected) {
                Log.d("HomeFragment", "Daten erhalten");
                if (movies.size() == 0)
                    errorCall("Ups, da ist was schiefgelaufen, versuche es später noch mal!");
                else {
                    try {
                        String releaseDate = "**Erscheinungsdatum:**  ";
                        String description = "**Beschreibung:**  ";
                        Log.d("movies", "movie1: " + movies.get(0).toString());
                        Log.d("movies", "movie2: " + movies.get(1).toString());
                        Log.d("movies", "movie3: " + movies.get(2).toString());
                        Log.d("movies", "movie4: " + movies.get(3).toString());
                        Log.d("movies", "movie5: " + movies.get(4).toString());
                        for (int i = 0; i < 5; i++) {
                            if (movies.get(i).getTitle() == null || movies.get(i).getTitle().equals("null"))
                                movies.get(i).setTitle("Daten nicht vorhanden.");
                            if (movies.get(i).getYear() == null || movies.get(i).getYear().equals("null"))
                                movies.get(i).setYear("Daten nicht vorhanden.");
                            if (movies.get(i).getDescription() == null || movies.get(i).getDescription().equals("null"))
                                movies.get(i).setDescription("Daten nicht vorhanden.");
                            if (movies.get(i).getPoster() == null || movies.get(i).getPoster().equals("null"))
                                movies.get(i).setPoster("https://icons.iconarchive.com/icons/icons8/windows-8/256/City-No-Camera-icon.png");
                        }
                        moviePoster = new ArrayList<>();
                        for (int i = 0; i < 5; i++) {
                            moviePoster.add(movies.get(i).getPoster());
                        }
                        new LoadImage().execute();

                        setMarkdown(movies.get(0).getTitle(), topMovieTitle1);
                        setMarkdown(description + movies.get(0).getDescription(), topMovieDescription1);
                        setMarkdown(releaseDate + movies.get(0).getYear(), topMovieYear1);
                        topMoviePoster1.setImageResource(R.drawable.ic_top_movie_no_image);

                        topMovieTitle2.setText(movies.get(1).getTitle());
                        setMarkdown(description + movies.get(1).getDescription(), topMovieDescription2);
                        setMarkdown(releaseDate + movies.get(1).getYear(), topMovieYear2);
                        topMoviePoster2.setImageResource(R.drawable.ic_top_movie_no_image);

                        topMovieTitle3.setText(movies.get(2).getTitle());
                        setMarkdown(description + movies.get(2).getDescription(), topMovieDescription3);
                        setMarkdown(releaseDate + movies.get(2).getYear(), topMovieYear3);
                        topMoviePoster3.setImageResource(R.drawable.ic_top_movie_no_image);

                        topMovieTitle4.setText(movies.get(3).getTitle());
                        setMarkdown(description + movies.get(3).getDescription(), topMovieDescription4);
                        setMarkdown(releaseDate + movies.get(3).getYear(), topMovieYear4);
                        topMoviePoster4.setImageResource(R.drawable.ic_top_movie_no_image);

                        topMovieTitle5.setText(movies.get(4).getTitle());
                        setMarkdown(description + movies.get(4).getDescription(), topMovieDescription5);
                        setMarkdown(releaseDate + movies.get(4).getYear(), topMovieYear5);
                        topMoviePoster5.setImageResource(R.drawable.ic_top_movie_no_image);
                    } catch (IndexOutOfBoundsException e) {
                        Log.e("HomeFragment", "Sorry, something went wrong" + e.getMessage());
                    }
                }
            } else
                errorCall("Ups, da ist was schiefgelaufen, überprüfe deine Internetverbindung!");
        }


        /**
         * parses markdown string and set it to text view
         *
         * @param string   string to be parsed
         * @param textView view to set the parsed string
         */

        private void setMarkdown(String string, TextView textView) {
            try {
                final Markwon markwon = Markwon.create(getContext());
                final Node node = markwon.parse(string);
                markwon.render(node);
                markwon.setMarkdown(textView, string);
            } catch (NullPointerException e) {
                Log.e("HomeFragment", "Error while parsing Markdown: " + e.getMessage());
            }
        }


        /**
         * sets title, request duration, image, release year and description of the movie
         *
         * @param movie  movie to get additional information
         * @param client OkHttpClient
         * @return additional information of movie
         * @throws IOException if additional information can not be loaded from imdb database
         */

        public Movie getAdditionalInformation(Movie movie, OkHttpClient client) throws IOException {
            Request requestMovieInformation = new Request.Builder()
                    .url("https://imdb8.p.rapidapi.com/title/get-overview-details?tconst=" + movie.getImdbId() + "&currentCountry=DE")
                    .get()
                    .addHeader("x-rapidapi-key", "eebb1996bbmsh17770f2e35a3f8dp18c9d8jsn7bfad2dd8a80")
                    .addHeader("x-rapidapi-host", "imdb8.p.rapidapi.com")
                    .build();

            Response response = client.newCall(requestMovieInformation).execute();
            JSONObject jsonObj;
            try {
                String string = response.body().string();
                jsonObj = new JSONObject(string);
                Log.d("json", "reponseBody: " + string);
                JSONObject title = jsonObj.getJSONObject("title");
                JSONObject plotOutline = jsonObj.getJSONObject("plotOutline");
                JSONObject image = title.getJSONObject("image");
                movie.setTitle(title.getString("title"));
                movie.setPoster(image.getString("url"));
                movie.setYear(jsonObj.getString("releaseDate"));
                movie.setDescription(plotOutline.getString("text"));
            } catch (JSONException e) {
                Log.e("HomeFragment", "Additional Information: Json parsing failed with IMDB_ID: " + movie.getImdbId() + " " + e.getMessage());
            }
            return movie;
        }

        
        public void errorCall(String message) {
            topMoviesLayout.setVisibility(View.GONE);
            movieErrorText.setVisibility(View.VISIBLE);
            reloadButton.setVisibility(View.VISIBLE);
            movieErrorText.setText(message);
            reloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new GetData().execute();
                    movieErrorText.setVisibility(View.GONE);
                    reloadButton.setVisibility(View.GONE);
                }
            });
        }
    }


    /**
     * load movie image asynchronously
     */

    public class LoadImage extends AsyncTask<Void, Void, ArrayList<RequestCreator>> {
        @Override
        protected ArrayList<RequestCreator> doInBackground(Void... voids) {
            int width = 450;
            int height = 600;

            ArrayList<RequestCreator> pictures = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                pictures.add(Picasso.get().load(moviePoster.get(i)).resize(width, height));
            }
            return pictures;
        }


        protected void onPostExecute(ArrayList<RequestCreator> pictures) {
            pictures.get(0).into(topMoviePoster1);
            pictures.get(1).into(topMoviePoster2);
            pictures.get(2).into(topMoviePoster3);
            pictures.get(3).into(topMoviePoster4);
            pictures.get(4).into(topMoviePoster5);
        }
    }
}