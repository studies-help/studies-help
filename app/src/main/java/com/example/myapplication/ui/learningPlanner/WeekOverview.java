package com.example.myapplication.ui.learningPlanner;

import android.content.Context;
import android.util.Log;

import com.example.myapplication.data.EventLearningPlanner;
import com.example.myapplication.ui.learningPlanner.Calendar.Day;
import com.example.myapplication.ui.learningPlanner.Calendar.Hour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class to administrate a Week and their events
 */
public class WeekOverview {
    List<TimeStamp> timestamps = new ArrayList<>();
    final EventLearningPlannerDataSource dataSource;
    private List<Day> days;

    public List<TimeStamp> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(List<TimeStamp> timestamps) {
        this.timestamps = timestamps;
    }


    /**
     * create For one Week List with TimeStamp and events for specific hour. It uses empty Events when on one day isn't an event or there're less events than on other days.
     *
     * @param context context of the OverView
     * @param week    List of the days to show
     */

    public WeekOverview(Context context, List<Day> week) {
        dataSource = new EventLearningPlannerDataSource(context);
        Integer c;
        this.days = week;

        for (int counter = 1; counter < 25; counter++) {
            List<EventLearningPlanner> events = new ArrayList<>();

            List<EventLearningPlanner> actuallyEventFirstDay;
            actuallyEventFirstDay = dataSource.getEventDuringHour(week.get(0).getCompleteDate(), getHourAsString(counter));
            List<EventLearningPlanner> actuallyEventSecondDay;
            actuallyEventSecondDay = dataSource.getEventDuringHour(week.get(1).getCompleteDate(), getHourAsString(counter));
            List<EventLearningPlanner> actuallyEventThirdDay;
            actuallyEventThirdDay = dataSource.getEventDuringHour(week.get(2).getCompleteDate(), getHourAsString(counter));
            List<EventLearningPlanner> actuallyEventFourthDay;
            actuallyEventFourthDay = dataSource.getEventDuringHour(week.get(3).getCompleteDate(), getHourAsString(counter));
            List<EventLearningPlanner> actuallyEventFifthDay;
            actuallyEventFifthDay = dataSource.getEventDuringHour(week.get(4).getCompleteDate(), getHourAsString(counter));
            List<EventLearningPlanner> actuallyEventSixDay;
            actuallyEventSixDay = dataSource.getEventDuringHour(week.get(5).getCompleteDate(), getHourAsString(counter));
            List<EventLearningPlanner> actuallyEventSevenDay;
            actuallyEventSevenDay = dataSource.getEventDuringHour(week.get(6).getCompleteDate(), getHourAsString(counter));

            int[] eventCounter = {actuallyEventFirstDay.size(), actuallyEventSecondDay.size(), actuallyEventThirdDay.size(), actuallyEventFourthDay.size(), actuallyEventFifthDay.size(), actuallyEventSixDay.size(), actuallyEventSevenDay.size(), 1};
            Arrays.sort(eventCounter);
            Log.d("WeekOverview", "Found  max" + eventCounter[eventCounter.length - 1] + " events per day in Time " + counter);
            for (c = 0; c < eventCounter[eventCounter.length - 1]; c++) {
                events.add(getRightEvent(actuallyEventFirstDay, c));
                events.add(getRightEvent(actuallyEventSecondDay, c));
                events.add(getRightEvent(actuallyEventThirdDay, c));
                events.add(getRightEvent(actuallyEventFourthDay, c));
                events.add(getRightEvent(actuallyEventFifthDay, c));
                events.add(getRightEvent(actuallyEventSixDay, c));
                events.add(getRightEvent(actuallyEventSevenDay, c));
            }
            Log.d("WeekOverview", "" + events.size());
            timestamps.add(new TimeStamp(new Hour(counter).getFullHour(), events));
        }
    }


    public String getHourAsString(Integer time) {
        return new Hour(time).getOnlyHour();
    }


    /**
     * Give empty event, when there isn't a real event.
     *
     * @param events events of the Week
     * @param round  round of creation
     * @return found event or Dummy-Event
     */

    public EventLearningPlanner getRightEvent(List<EventLearningPlanner> events, Integer round) {
        if (events.size() <= round || events.size() == 0)
            return new EventLearningPlanner("", "", "", "", "", "", "");
        else {
            Log.d("WeekOverview", "Event found");
            return events.get(round);
        }
    }

    public EventLearningPlannerDataSource getDataSource() {
        return dataSource;
    }

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }
}
