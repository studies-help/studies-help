package com.example.myapplication.ui.courses;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.data.AppDatabase;
import com.example.myapplication.data.Course;
import com.example.myapplication.data.DbSingleton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CoursesFragment extends Fragment {
    private List<Course> dataList = new ArrayList<>();
    private AppDatabase database;
    private CourseAdapter adapter;
    private Course course;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() == null)
            return root;
        root = inflater.inflate(R.layout.fragment_courses, container, false);
        FloatingActionButton btAddOpenWindow = getActivity().findViewById(R.id.fab);
        btAddOpenWindow.setImageResource(R.drawable.ic_add);
        btAddOpenWindow.setVisibility(View.VISIBLE);
        RecyclerView recyclerView = root.findViewById(R.id.courseRecyclerView);
        database = DbSingleton.instance(getContext()).getAppDatabase();
        dataList = database.courseDao().getAll();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new CourseAdapter(getActivity(), dataList);
        recyclerView.setAdapter(adapter);

        btAddOpenWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.fragment_courses_dialog_add_course);
                int width = WindowManager.LayoutParams.MATCH_PARENT;
                int height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setLayout(width, height);
                dialog.show();

                Button btAdd = dialog.findViewById(R.id.courseAddButton);
                Button btAbort = dialog.findViewById(R.id.courseAbortButton);
                EditText etCourseName = dialog.findViewById(R.id.courseCourseNameEditText);
                EditText etLecturer = dialog.findViewById(R.id.courseLecturerEditText);
                EditText etCourseRoom = dialog.findViewById(R.id.courseCourseRoomEditText);
                EditText etContactData = dialog.findViewById(R.id.courseContactDataEditText);
                EditText etFellowStudents = dialog.findViewById(R.id.courseFellowStudentsEditText);

                btAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Get String from edit text
                        String cnText = etCourseName.getText().toString().trim();
                        String lText = etLecturer.getText().toString().trim();
                        String crText = etCourseRoom.getText().toString().trim();
                        String cdText = etContactData.getText().toString().trim();
                        String fsText = etFellowStudents.getText().toString().trim();

                        //Check condition
                        if (!cnText.equals("")) {
                            course = new Course(cnText, lText, crText, cdText, fsText);
                            database.courseDao().insert(course);

                            etCourseName.setText("");
                            etLecturer.setText("");
                            etLecturer.setText("");
                            etContactData.setText("");
                            etFellowStudents.setText("");
                            dataList.clear();
                            dataList.addAll(database.courseDao().getAll());
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                        else {
                            new AlertDialog.Builder(requireContext())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Fehler")
                                    .setMessage("Bitte fügen Sie einen Kursnamen ein.")
                                    .setPositiveButton("Ok", null)
                                    .setNegativeButton("Entwurf löschen", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            etCourseName.setText("");
                                            etLecturer.setText("");
                                            etLecturer.setText("");
                                            etContactData.setText("");
                                            etFellowStudents.setText("");

                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }

                    }
                });

                btAbort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etCourseName.setText("");
                        etLecturer.setText("");
                        etLecturer.setText("");
                        etContactData.setText("");
                        etFellowStudents.setText("");

                        dialog.dismiss();
                    }
                });
            }
        });
        return root;
    }
}