package com.example.myapplication.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "learningCard"
//        foreignKeys = @ForeignKey(entity = Course.class,
//        parentColumns = "courseID",
//        childColumns = "learningCardID",
//        onUpdate = CASCADE,
//        onDelete = CASCADE)
)

public class LearningCard implements Serializable {
    @PrimaryKey(autoGenerate = true)
    long learningCardID;
    String learningCardTitle;
    String learningCardContent;
    int learningCardContentAlignment;

    public LearningCard() {
    }

    public LearningCard(long learningCardId, String content, String title, int contentAlignment) {
        this.learningCardID = learningCardId;
        this.learningCardContent = content;
        this.learningCardTitle = title;
        this.learningCardContentAlignment = contentAlignment;
    }

    public long getLearningCardID() {
        return learningCardID;
    }

    public void setLearningCardID(long learningCardId) {
        this.learningCardID = learningCardId;
    }

    public String getLearningCardContent() {
        return learningCardContent;
    }

    public void setLearningCardContent(String content) {
        this.learningCardContent = content;
    }

    public String getLearningCardTitle() {
        return learningCardTitle;
    }

    public void setLearningCardTitle(String title) {
        this.learningCardTitle = title;
    }

    public int getLearningCardContentAlignment() {
        return learningCardContentAlignment;
    }

    public void setLearningCardContentAlignment(int contentAlignment) {
        this.learningCardContentAlignment = contentAlignment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof LearningCard))
            return false;

        LearningCard learningCard = (LearningCard) o;

        if (learningCardID != learningCard.learningCardID)
            return false;
        if (learningCardTitle != null)
            return learningCardTitle.equals(learningCard.learningCardTitle);
        else
            return learningCard.learningCardTitle == null;
    }

    @Override
    public int hashCode() {
        int result = (int) learningCardID;
        result = 31 * result + (learningCardTitle != null ? learningCardTitle.hashCode() : 0);
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        return learningCardTitle;
    }
}
