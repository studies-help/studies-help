package com.example.myapplication.data;

import android.content.ContentValues;
import android.content.Context;
import android.view.Gravity;

import androidx.annotation.NonNull;
import androidx.room.OnConflictStrategy;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.myapplication.ui.learningPlanner.Calendar.Day;

public class DbSingleton {
    private static DbSingleton instance = null;
    private static final String DB_NAME = "database";

    private final AppDatabase db;

    private DbSingleton(Context context) {
        db = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class,
                DB_NAME)
                .allowMainThreadQueries()
                .addCallback(rdc)
                .build();
    }

    public AppDatabase getAppDatabase() {
        return db;
    }

    public static synchronized DbSingleton instance(Context context) {
        if (instance == null) {
            instance = new DbSingleton(context);
        }
        return instance;
    }

    final RoomDatabase.Callback rdc = new RoomDatabase.Callback() {
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            Day day = new Day();
            String date = day.getCompleteDate();
            day.setAnOtherDate(1);

            insertEvent(db, "EventLearningplanner", "Netflix & Chill", date, "20:00", "23:59", "Couch", "Vor Klausurvorbereitung Drücken");
            insertEvent(db, "EventLearningplanner", "Stand-Up", date, "08:00", "08:30", "Zoom", "Android Runde");
            insertEvent(db, "EventLearningplanner", "Runder Tisch", date, "08:35", "09:30", "Zoom", "Mikrofon deaktiveren nicht vergessen!");

            insertCourse(db, "course", "Logik", "Strotmann", "Discord", "via Discord", "Georg Büchner");
            insertCourse(db, "course", "BWL", "Müller", "Blackboard", "exampleData@example.de", "Karl Koch");
            insertCourse(db, "course", "Java", "Koch", "Discord", "exampleData@example.de", "Christian Drosten");

            insertLearningCards(db, "learningCard", "Grundrechenarten",
                    "Addition (+), Subtraktion (-), Multiplikation (*), Division (/)", Gravity.START);
            insertLearningCards(db, "learningCard", "Führungsstile",
                    "**kooperativ**, laissez-faire, autoritär, kumpelhaft", Gravity.END);
            insertLearningCards(db, "learningCard", "Einführung Java",
                    "objekt orientiert (OO)\nerstes Release: 1995 (beta), 1996 (normal)", Gravity.CENTER_HORIZONTAL);
        }
        // do something after database has been created


        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            // do something every time database is open
        }
    };

    public void insertEvent(SupportSQLiteDatabase db, String table, String eventName, String eventDate, String eventStarTime, String eventEndTime, String eventPlace, String evenDescription) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("eventName", eventName);
        contentValues.put("eventDate", eventDate);
        contentValues.put("eventStartTime", eventStarTime);
        contentValues.put("eventEndTime", eventEndTime);
        contentValues.put("eventPlace", eventPlace);
        contentValues.put("eventDescription", evenDescription);
        db.insert(table, OnConflictStrategy.IGNORE, contentValues);
    }

    public void insertCourse(SupportSQLiteDatabase db, String table, String courseName, String lecturer, String courseRoom, String contactData, String fellowStudents) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("courseName", courseName);
        contentValues.put("lecturer", lecturer);
        contentValues.put("courseRoom", courseRoom);
        contentValues.put("contactData", contactData);
        contentValues.put("fellowStudents", fellowStudents);
        db.insert(table, OnConflictStrategy.IGNORE, contentValues);
    }

    public void insertLearningCards(SupportSQLiteDatabase db, String table, String learningCardTitle, String learningCardContent, Integer learningCardAlignment) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("learningCardTitle", learningCardTitle);
        contentValues.put("learningCardContent", learningCardContent);
        contentValues.put("learningCardContentAlignment", learningCardAlignment);
        db.insert(table, OnConflictStrategy.IGNORE, contentValues);
    }
}
