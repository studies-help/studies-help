package com.example.myapplication.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LearningCardDao {
    @Query("SELECT * FROM learningCard")
    List<LearningCard> getLearningCards();

    @Insert
    long insert(LearningCard learningCard);

    @Update
    void onUpdate(LearningCard learningCard);

    @Delete
    void delete(LearningCard learningCard);
}
