package com.example.myapplication.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface EventLearningPlannerDao {
    @Query("SELECT * FROM EventLearningPlanner")
    List<EventLearningPlanner> loadAll();

    @Query("SELECT * FROM EventLearningPlanner WHERE eventDate == :eventDate ORDER BY EventStartTime")
    List<EventLearningPlanner> loadEvents(String eventDate);

    @Query("SELECT * FROM EventLearningPlanner WHERE eventDate== :eventDate and (substr(:eventTime, 1,2) BETWEEN substr(eventStartTime, 1,2) and (substr(eventEndTime,1,2))) ORDER BY EventStartTime")
    List<EventLearningPlanner> loadEventsDuringHour(String eventDate, String eventTime);

    @Query("SELECT * FROM EventLearningPlanner WHERE eventDate== :eventDate and substr(eventStartTime, 1,2) == :eventTime ORDER BY EventStartTime")
    List<EventLearningPlanner> loadEventsStartAtHour(String eventDate, String eventTime);

    @Query("SELECT * FROM EventLearningPlanner WHERE eventDate== :eventDate and " +
            "(((substr(:eventStartTime, 1,2) < substr(eventStartTime, 1,2) or (substr(:eventStartTime, 1,2) = substr(eventStartTime, 1,2) and substr(:eventStartTime, 4,2) <= substr(eventStartTime, 4,2))) and (substr(:eventEndTime, 1,2) > substr(eventStartTime, 1,2) or (substr(:eventEndTime, 1,2) = substr(eventStartTime, 1,2) and substr(:eventEndTime, 4,2) >= substr(eventStartTime, 4,2) )))or" +
            " ((substr(:eventStartTime, 1,2) > substr(eventStartTime, 1,2) or (substr(:eventStartTime, 1,2) = substr(eventStartTime, 1,2) and substr(:eventStartTime, 4,2) >= substr(eventStartTime, 4,2)))and (substr(:eventStartTime, 1,2) <substr(eventEndTime, 1,2) or (substr(:eventStartTime, 1,2) == substr(eventEndTime, 1,2) and substr(:eventStartTime, 4,2) <= substr(eventEndTime, 4,2) )) )" +
            "  ) ORDER BY EventStartTime")
    List<EventLearningPlanner> loadEventsDuringTimePeriod(String eventDate, String eventStartTime, String eventEndTime);


    @Insert
    void insertAll(EventLearningPlanner... event);

    @Update
    void updateAll(EventLearningPlanner... event);

    @Delete
    void delete(EventLearningPlanner event);
}
