package com.example.myapplication.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "course")
public class Course implements Serializable {
    @PrimaryKey(autoGenerate = true)
    int courseID;
    String courseName;
    String lecturer;
    String courseRoom;
    String contactData;
    String fellowStudents;

    public Course() {
    }

    public Course(String courseName, String lecturer, String courseRoom, String contactData,
                  String fellowStudents){
        this.courseName = courseName;
        this.lecturer = lecturer;
        this.courseRoom = courseRoom;
        this.contactData = contactData;
        this.fellowStudents = fellowStudents;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getCourseRoom() {
        return courseRoom;
    }

    public void setCourseRoom(String courseRoom) {
        this.courseRoom = courseRoom;
    }

    public String getContactData() {
        return contactData;
    }

    public void setContactData(String contactData) {
        this.contactData = contactData;
    }

    public String getFellowStudents() {
        return fellowStudents;
    }

    public void setFellowStudents(String fellowStudents) {
        this.fellowStudents = fellowStudents;
    }
}
