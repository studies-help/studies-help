package com.example.myapplication.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CourseDao {
    @Insert(onConflict = REPLACE)
    void insert(Course course);

    @Delete
    void delete(Course course);

    @Query("DELETE FROM course WHERE courseID = :sID")
    void deleteDB(int sID);

    @Query("UPDATE course SET courseName = :sText WHERE courseID = :sID")
    void updateCourseName(int sID,String sText);

    @Query("UPDATE course SET lecturer = :sText WHERE courseID = :sID")
    void updateLecturer(int sID,String sText);

    @Query("UPDATE course SET courseRoom = :sText WHERE courseID = :sID")
    void updateCourseRoom(int sID,String sText);

    @Query("UPDATE course SET contactData = :sText WHERE courseID = :sID")
    void updateContactData(int sID,String sText);

    @Query("UPDATE course SET fellowStudents = :sText WHERE courseID = :sID")
    void updateFellowStudents(int sID,String sText);

    @Query("SELECT * FROM course WHERE courseID== :courseID")
    List<Course> getCourse (Integer courseID);

    @Query("SELECT * FROM course")
    List<Course> getAll();
}
