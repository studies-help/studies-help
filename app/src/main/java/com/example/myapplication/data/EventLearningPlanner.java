package com.example.myapplication.data;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.SET_NULL;

@Entity
(foreignKeys = @ForeignKey(entity = Course.class,
        parentColumns = "courseID",
        childColumns = "courseID",
        onDelete = SET_NULL))
public class EventLearningPlanner {
    @PrimaryKey(autoGenerate = true)
    int eventID;
    Integer courseID;
    String eventName;
    String eventStartTime;
    String eventEndTime;
    String eventDate;
    String eventDescription;
    String eventPlace;
    String eventInvolved;

    public EventLearningPlanner(String eventName, String eventStartTime, String eventEndTime, String eventDate, String eventDescription, String eventInvolved, String eventPlace) {
        this.eventName = eventName;
        this.eventStartTime = eventStartTime;
        this.eventEndTime = eventEndTime;
        this.eventDate = eventDate;
        this.eventDescription = eventDescription;
        this.eventPlace = eventPlace;
        this.eventInvolved = eventInvolved;

    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public String getEventInvolved() {
        return eventInvolved;
    }

    public void setEventInvolved(String eventInvolved) {
        this.eventInvolved = eventInvolved;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public Integer getCourseID() {
        return courseID;
    }

    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public String getEventCompleteTime() {
        return eventStartTime + "-" + eventEndTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }


}
