# Studies Help
Dies ist eine Android Applikation, die Studierende nutzen können, um Vorlesungen und Lernphasen einzutragen. Um Vorlesungen zusammenzufassen, können Lernkarten erstellt werden.

# Unterstütze APIs
Android 9.0 bis 10.0 (Api Level: 28-29)

# Unterstützte Geräte
Die App sollte auf allen Smarphones verwendbar sein, die ein normales Seitenverhältnisse haben. Entwickelt und getestet wurde die App auf Google Pixel 2, 3 und 4.

# Beispieldaten
Bei der ersten Installation werden Beispieldaten für Lernplaner, Kursdaten und Lernkarten erstellt. Hierbei ist zu beachten, das die Beispieldaten vom Lernplaner für den aktuellen Tag der Installation gesetzt werden.

# Features
## Lernplaner
- Bietet die Möglichkeit, Termine zu erstellen und wenn es sich um eine Lernaktivität handelt, diese durch eine Kurszuordnung und eventuellen andere Beteiligte zu ergänzen.
### Features
- Der Kalender überprüft beim Termin Erstellen automatisch, ob in dieser Zeit bereit etwas geplant wurde und verhindert so das überlagern von Terminen.
- Der Kalender springt automatisch zu der Woche, wo der geplante Termin stattfindet (Terminwahl nur zwischen 1900 und 2100 möglich).
- Zum Schutz eines geregelten Arbeitstages darf kein Termin über Mitternacht hinausgehen oder über mehrer Tage gehen.
- Termin lassen sich per Draufklicken in der Detailansicht betrachten. Außerdem kann man von dieser Ansicht aus in das Editieren-Fenster kommen
- Löschen eines Termin geht nur vom Editierungs-Fenster aus.
- Der Kalender  stellt immer 7 Tage einer gewählten Woche dar. Wobei der ersten Wochentag (z.B Montag) immer der aktuelle Wochentag ist. Man kann per Klick an den jeweiligen Seiten 7 Tage vor oder zurück gehen. Der gewählte Wochentag bleibt dabei erhalten (Dienstag der 7. wird gewählt und  in der nächsten Woche ist Dienstag der 14. gewählt).
- Zur aktuellen Woche lässt sich immer über den “Zurücksetzen auf Heute “-Button zurückkehren.
- Für die Eventansicht kann man zwischen einer Wochen- oder einer Tagesübersicht wählen. 
- Die Tagesübersichten werden immer per Klick auf den entsprechenden Wochentag geladen. Befindet man sich bei der Auswahl in der Wochenübersicht wird die Tagesübersicht erst bei wechseln der Ansicht geladen.
- Der aktuelle Tag und die aktuelle Stunden werden Orange markiert.

## Kursdaten
- Bietet die Möglichkeit, Kurse und dazugehörige Daten, wie Kursnamen, Dozent, Kursraum, Kontaktdaten und Kommilitonen zu speichern.
### Features
- Um Kurs hinzuzufügen auf den grünen “+”-Button drücken.
- anschauen der Kursdaten
- editieren der Kursdaten
- löschen der Kursdaten

## Lernkarten
- Lernkarten Editor, dessen Text mithilfe von Markdown formatiert werden kann.
### unterstützte Markdown Features 
- \*text\*, \_text\_: kursiv
- \*\*text\*\*, \_\_text\_\_: fett
- \*\*\*text\*\*\*, \_\_\_text\_\_\_: fett und kursiv
- \[Link\]\(URL\): Links
- \-\-\- , \*\*\*, \_\_\_: Absatz
- \# Titel1 , \#\# Titel2 , \#\#\# Titel3 ,... : Überschrift 1, 2, 3, … (unterstützt bis Überschrift 6)
- \> Zitat: Blockzitat
- 1\. Element1 , 2\. Element2, … : sortierte Liste
- \- unsortiert: unsortierte Liste
- \`code\`: code
- mindestens 2 Leerzeichen: neue Zeile
### Features
- Geparster Markdown-Text wird in einer Vorschau angezeigt.
- Es können sowohl neue Lernkarten erstellt (Klick auf den grünen Lernkarten-Plus-Button) als auch bestehende Lernkarten editiert (Klick auf die entsprechende Lernkarte) werden.
- Um den Überblick auch bei vielen Lernkarten zu behalten, können diese durchsucht werden (Klick auf die Lupe).

## Top Filme
- Immer top informiert über die 5 populärste Filme der Welt.
- Zusätzliche Informationen wie Erscheinungsdatum, Beschreibung und Titelbild werden beim Klick auf den Titel oder die Pfeile ausgeklappt.

# API-Key
Zur Zeit der Abgabe der App sind auf dem master branch noch 99 Requests in der App übrig. Sollten keine Meldungen im Top Filme Fragment angezeigt werden, folgen Sie bitte der Anleitung, um einen neuen Api-Key zu generieren.   
- bei [rapidapi](https://rapidapi.com/apidojo/api/imdb8) einloggen (z.B. über ein Google-Konto)
- bei Pricing einen Plan auswählen (mit dem kostenfreien Plan können 100 Anfragen im Monat in der App gemacht werden)   
![Bild](https://gitlab.com/studies-help/studies-help/-/blob/develop/Pricing_rapidApi.png "pricing")
- Java -> OkHttp auswählen   
![Bild](https://gitlab.com/studies-help/studies-help/-/blob/develop/OkHttp_rapidApi.png "Java_OkHttp")
- Zeile 6-7 kopieren und in TopMovieFragment.java in Zeile 292-293 und 415-416 einfügen   
  (.addHeader("x-rapidapi-key", "b499c003d4mshdb07034a7a55ae4p19de03jsn8058c212c676")    
   .addHeader("x-rapidapi-host", "imdb8.p.rapidapi.com")   
) -> Beispiel


# Bibliotheken
## Lernkarten
- [Marwon](https://github.com/noties/Markwon)
## Top Filme
- [Picasso](https://github.com/square/picasso)
- [OkHttp](https://github.com/square/okhttp)   

[README zum Bearbeiten](https://docs.google.com/document/d/1zbXSpulES4frZV6M9hRgM6fP-V9pabE7Am79bJ5lvu8/edit?usp=sharing)   
